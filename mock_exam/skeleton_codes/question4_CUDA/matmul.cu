#include <cstdio>
#include "utils.h"

constexpr int BLOCK_SIZE = 16;
using real = float;

__global__ void matMulKernel1(int N, const real *a, const real *b, real *c) {
    // TODO: Implement the basic kernel.
    (void)N;
    (void)a;
    (void)b;
    (void)c;
}

__global__ void matMulKernel2(int N, const real *a, const real *b, real *c) {
    // TODO: Implement blocking.
    (void)N;
    (void)a;
    (void)b;
    (void)c;
}

__global__ void matMulKernel3(int N, const real *a, const real *b, real *c) {
    // TODO: Make each thread compute 4 output cells (no blocking).
    (void)N;
    (void)a;
    (void)b;
    (void)c;
}

__global__ void matMulKernel4(int N, const real *a, const real *b, real *c) {
    // TODO: Combine blocking and instruciton-level parallelism.
    (void)N;
    (void)a;
    (void)b;
    (void)c;
}

void matMulCPU(int N, const real *a, const real *b, real *c) {
    for (int iy = 0; iy < N; ++iy)
    for (int ix = 0; ix < N; ++ix) {
        real sum = 0;
        for (int k = 0; k < N; ++k)
            sum += a[iy * N + k] * b[k * N + ix];
        c[iy * N + ix] = sum;
    }
}

void matMulGPU(int N, const real *aDev, const real *bDev, real *cDev, int algo) {
    if (N % (BLOCK_SIZE * 4) != 0) {
        fprintf(stderr, "Expected N % (4 * BLOCK_SIZE) == 0\n");
        exit(1);
    }

    if (algo == 1) {
        // TODO: invoke the kernel.
        (void)aDev;
        (void)bDev;
        (void)cDev;
    } else if (algo == 2) {
        // TODO: invoke the kernel.
    } else if (algo == 3) {
        // TODO: invoke the kernel.
    } else if (algo == 4) {
        // TODO: invoke the kernel.
    } else {
        fprintf(stderr, "Unrecognized algorithm %d\n", algo);
        exit(1);
    }
}


void benchmarkMatrixMultiplication(int N, bool check, int algo) {
    real *aHost;
    real *bHost;
    real *cHost;
    real *aDev;
    real *bDev;
    real *cDev;

    // Allocate.
    CUDA_CHECK(cudaMallocHost(&aHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMallocHost(&bHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMallocHost(&cHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&aDev, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&bDev, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&cDev, N * N * sizeof(real)));

    // Prepare A and B.
    for (int iy = 0; iy < N; ++iy)
    for (int ix = 0; ix < N; ++ix) {
        aHost[iy * N + ix] = iy + ix;
        // bHost[iy * N + ix] = ix * ix + iy;
        bHost[iy * N + ix] = ix + iy;
    }
    CUDA_CHECK(cudaMemcpy(aDev, aHost, N * N * sizeof(real), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(bDev, bHost, N * N * sizeof(real), cudaMemcpyHostToDevice));

    // Compute C = A * B on GPU.
    double dt = benchmark(100, [=]() {
        matMulGPU(N, aDev, bDev, cDev, algo);
    });
    double gflops = 1e-9 * 2LL * N * N * N / dt;
    printf("N=%d   GFLOP/s=%.1f\n", N, gflops);

    // Check correctnes.
    if (check) {
        matMulCPU(N, aHost, bHost, cHost);
        real *tmpHost = aHost;
        CUDA_CHECK(cudaMemcpy(tmpHost, cDev, N * N * sizeof(real), cudaMemcpyDeviceToHost));
        for (int iy = 0; iy < N; ++iy)
        for (int ix = 0; ix < N; ++ix) {
            if (tmpHost[iy * N + ix] != cHost[iy * N + ix]) {
                fprintf(stderr, "Incorrect result at [%d][%d] --> host=%f gpu=%f\n",
                        iy, ix, cHost[iy * N + ix], tmpHost[iy * N + ix]);
                exit(1);
            }
        }
        printf("GPU result correct.\n");
    }

    // Deallocate.
    CUDA_CHECK(cudaFree(cDev));
    CUDA_CHECK(cudaFree(bDev));
    CUDA_CHECK(cudaFree(aDev));
    CUDA_CHECK(cudaFreeHost(cHost));
    CUDA_CHECK(cudaFreeHost(bHost));
    CUDA_CHECK(cudaFreeHost(aHost));
}


int main(int argc, char **argv) {
    int algo = 1;
    if (argc >= 2)
        sscanf(argv[1], "%d", &algo);
    benchmarkMatrixMultiplication(256, true, algo);
    benchmarkMatrixMultiplication(3072, false, algo);
}
