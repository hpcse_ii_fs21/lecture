#include <cstdio>
#include "utils.h"

constexpr int BLOCK_SIZE = 16;
using real = float;

__global__ void matMulKernel1(int N, const real *a, const real *b, real *c) {
    int ix = blockIdx.x * blockDim.x + threadIdx.x;
    int iy = blockIdx.y * blockDim.y + threadIdx.y;

    // No check needed if N % BLOCK_SIZE == 0.
    // if (ix >= N || iy >= N)
    //     return;

    real sum = 0;
    for (int k = 0; k < N; ++k)
        sum += a[iy * N + k] * b[k * N + ix];
    c[iy * N + ix] = sum;
}

__global__ void matMulKernel2(int N, const real *a, const real *b, real *c) {
    int ix = blockIdx.x * blockDim.x + threadIdx.x;
    int iy = blockIdx.y * blockDim.y + threadIdx.y;

    __shared__ real A[BLOCK_SIZE][BLOCK_SIZE];
    __shared__ real B[BLOCK_SIZE][BLOCK_SIZE];

    real sum = 0;

    // Original code.
    // for (int k = 0; k < N; ++k)
    //     sum += a[iy * N + k] * b[k * N + ix];

    // "Blocking" without shared memory.
    // for (int k0 = 0; k0 < N; k0 += BLOCK_SIZE) {
    //     for (int k = 0; k < BLOCK_SIZE; ++k)
    //         sum += a[iy * N + (k + k0)] * b[(k0 + k) * N + ix];
    //     __syncthreads();
    // }

    for (int k0 = 0; k0 < N; k0 += BLOCK_SIZE) {
        A[threadIdx.y][threadIdx.x] = a[iy * N + k0 + threadIdx.x];
        B[threadIdx.y][threadIdx.x] = b[(k0 + threadIdx.y) * N + ix];

		__syncthreads();
        for (int k = 0; k < BLOCK_SIZE; ++k)
            sum += A[threadIdx.y][k] * B[k][threadIdx.x];
		__syncthreads();
    }

    c[iy * N + ix] = sum;
}

__global__ void matMulKernel3(int N, const real *a, const real *b, real *c) {
    int ix = blockIdx.x * blockDim.x + threadIdx.x;
    int iy = blockIdx.y * blockDim.y + threadIdx.y;
    int iy4 = 4 * iy;

    // if (ix >= N || iy4 >= N)
    //     return;

    real sum0 = 0;
    real sum1 = 0;
    real sum2 = 0;
    real sum3 = 0;
    for (int k = 0; k < N; ++k) {
        sum0 += a[(iy4 + 0) * N + k] * b[k * N + ix];
        sum1 += a[(iy4 + 1) * N + k] * b[k * N + ix];
        sum2 += a[(iy4 + 2) * N + k] * b[k * N + ix];
        sum3 += a[(iy4 + 3) * N + k] * b[k * N + ix];
    }
    c[(iy4 + 0) * N + ix] = sum0;
    c[(iy4 + 1) * N + ix] = sum1;
    c[(iy4 + 2) * N + ix] = sum2;
    c[(iy4 + 3) * N + ix] = sum3;
}

__global__ void matMulKernel4(int N, const real *a, const real *b, real *c) {
    int ix = blockIdx.x * blockDim.x + threadIdx.x;
    int iy = blockIdx.y * blockDim.y + threadIdx.y;
    int iy4 = 4 * iy;

    __shared__ real A[BLOCK_SIZE * 4][BLOCK_SIZE];
    __shared__ real B[BLOCK_SIZE][BLOCK_SIZE];

    real sum0 = 0;
    real sum1 = 0;
    real sum2 = 0;
    real sum3 = 0;

    for (int k0 = 0; k0 < N; k0 += BLOCK_SIZE) {
        A[4 * threadIdx.y + 0][threadIdx.x] = a[(iy4 + 0) * N + k0 + threadIdx.x];
        A[4 * threadIdx.y + 1][threadIdx.x] = a[(iy4 + 1) * N + k0 + threadIdx.x];
        A[4 * threadIdx.y + 2][threadIdx.x] = a[(iy4 + 2) * N + k0 + threadIdx.x];
        A[4 * threadIdx.y + 3][threadIdx.x] = a[(iy4 + 3) * N + k0 + threadIdx.x];
        B[threadIdx.y][threadIdx.x] = b[(k0 + threadIdx.y) * N + ix];

		__syncthreads();
        for (int k = 0; k < BLOCK_SIZE; ++k) {
            sum0 += A[4 * threadIdx.y + 0][k] * B[k][threadIdx.x];
            sum1 += A[4 * threadIdx.y + 1][k] * B[k][threadIdx.x];
            sum2 += A[4 * threadIdx.y + 2][k] * B[k][threadIdx.x];
            sum3 += A[4 * threadIdx.y + 3][k] * B[k][threadIdx.x];
        }
		__syncthreads();
    }

    c[(iy4 + 0) * N + ix] = sum0;
    c[(iy4 + 1) * N + ix] = sum1;
    c[(iy4 + 2) * N + ix] = sum2;
    c[(iy4 + 3) * N + ix] = sum3;
}

void matMulCPU(int N, const real *a, const real *b, real *c) {
    for (int iy = 0; iy < N; ++iy)
    for (int ix = 0; ix < N; ++ix) {
        real sum = 0;
        for (int k = 0; k < N; ++k)
            sum += a[iy * N + k] * b[k * N + ix];
        c[iy * N + ix] = sum;
    }
}

void matMulGPU(int N, const real *aDev, const real *bDev, real *cDev, int algo) {
    if (N % (BLOCK_SIZE * 4) != 0) {
        fprintf(stderr, "Expected N % (4 * BLOCK_SIZE) == 0\n");
        exit(1);
    }

    if (algo == 1) {
        dim3 threads(BLOCK_SIZE, BLOCK_SIZE, 1);
        dim3 blocks((N + threads.x - 1) / threads.x,
                    (N + threads.y - 1) / threads.y,
                    1);
        matMulKernel1<<<blocks, threads>>>(N, aDev, bDev, cDev);
    } else if (algo == 2) {
        dim3 threads(BLOCK_SIZE, BLOCK_SIZE, 1);
        dim3 blocks((N + threads.x - 1) / threads.x,
                    (N + threads.y - 1) / threads.y,
                    1);
        matMulKernel2<<<blocks, threads>>>(N, aDev, bDev, cDev);
    } else if (algo == 3) {
        dim3 threads(BLOCK_SIZE, BLOCK_SIZE, 1);
        dim3 blocks((N + threads.x - 1) / threads.x,
                    (N + threads.y * 4 - 1) / threads.y / 4,
                    1);
        matMulKernel3<<<blocks, threads>>>(N, aDev, bDev, cDev);
    } else if (algo == 4) {
        dim3 threads(BLOCK_SIZE, BLOCK_SIZE, 1);
        dim3 blocks((N + threads.x - 1) / threads.x,
                    (N + threads.y * 4 - 1) / threads.y / 4,
                    1);
        matMulKernel4<<<blocks, threads>>>(N, aDev, bDev, cDev);
    } else {
        fprintf(stderr, "Unrecognized algorithm %d\n", algo);
        exit(1);
    }
}


void benchmarkMatrixMultiplication(int N, bool check, int algo) {
    real *aHost;
    real *bHost;
    real *cHost;
    real *aDev;
    real *bDev;
    real *cDev;

    // Allocate.
    CUDA_CHECK(cudaMallocHost(&aHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMallocHost(&bHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMallocHost(&cHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&aDev, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&bDev, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&cDev, N * N * sizeof(real)));

    // Prepare A and B.
    for (int iy = 0; iy < N; ++iy)
    for (int ix = 0; ix < N; ++ix) {
        aHost[iy * N + ix] = iy + ix;
        // bHost[iy * N + ix] = ix * ix + iy;
        bHost[iy * N + ix] = ix + iy;
    }
    CUDA_CHECK(cudaMemcpy(aDev, aHost, N * N * sizeof(real), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(bDev, bHost, N * N * sizeof(real), cudaMemcpyHostToDevice));

    // Compute C = A * B on GPU.
    double dt = benchmark(100, [=]() {
        matMulGPU(N, aDev, bDev, cDev, algo);
    });
    double gflops = 1e-9 * 2LL * N * N * N / dt;
    printf("N=%d   GFLOP/s=%.1f\n", N, gflops);

    // Check correctnes.
    if (check) {
        matMulCPU(N, aHost, bHost, cHost);
        real *tmpHost = aHost;
        CUDA_CHECK(cudaMemcpy(tmpHost, cDev, N * N * sizeof(real), cudaMemcpyDeviceToHost));
        for (int iy = 0; iy < N; ++iy)
        for (int ix = 0; ix < N; ++ix) {
            if (tmpHost[iy * N + ix] != cHost[iy * N + ix]) {
                fprintf(stderr, "Incorrect result at [%d][%d] --> host=%f gpu=%f\n",
                        iy, ix, cHost[iy * N + ix], tmpHost[iy * N + ix]);
                exit(1);
            }
        }
        printf("GPU result correct.\n");
    }

    // Deallocate.
    CUDA_CHECK(cudaFree(cDev));
    CUDA_CHECK(cudaFree(bDev));
    CUDA_CHECK(cudaFree(aDev));
    CUDA_CHECK(cudaFreeHost(cHost));
    CUDA_CHECK(cudaFreeHost(bHost));
    CUDA_CHECK(cudaFreeHost(aHost));
}


int main(int argc, char **argv) {
    int algo = 1;
    if (argc >= 2)
        sscanf(argv[1], "%d", &algo);
    benchmarkMatrixMultiplication(256, true, algo);
    benchmarkMatrixMultiplication(3072, false, algo);
}
