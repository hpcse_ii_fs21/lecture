import numpy as np
from scipy.stats import beta
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

# Beta distribution with a=b=1 is the uniform distribution 

x = np.linspace(0, 1, 100)

a, b = 1, 1
rv = beta(a, b)
pdf = rv.pdf(x)

plt.plot(x, pdf, linewidth=2, label=r"$\alpha=\beta=1$")

plt.xlabel(r"$P_H$")
plt.ylabel(r"PDF $p(P_H)$")
plt.title(r"$Beta(\alpha, \beta)$")
plt.legend(loc="upper left", bbox_to_anchor=(1.05, 1), borderaxespad=0.)
plt.ylim([0,1.1])
# plt.tight_layout()
plt.savefig("0_uniform_prior.png", dpi=400, bbox_inches='tight')
# plt.show()
plt.close()

