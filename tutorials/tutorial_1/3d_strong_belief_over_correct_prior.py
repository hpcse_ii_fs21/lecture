import numpy as np
from scipy.stats import beta
import matplotlib
import matplotlib.pyplot as plt
import scipy.special

# True (unknown) probability of the coin toss coming HEADS
P_H = 0.8
x = np.linspace(0, 1, 1000)

def getPDFBeta(a, b):
    posterior = beta(a, b)
    pdf = posterior.pdf(x)
    return pdf

def getPDFBinomail(N_H, N):
    # The PDF of the Binomial with respect to p (not N_H)
    bn = scipy.special.binom(N, N_H)
    pdf = bn * np.power(x, N_H) * np.power(1-x, N-N_H)
    return pdf

def plotExperiment(prior, posterior, N, N_H):
    pdf_prior = getPDFBeta(prior[0], prior[1])
    pdf_posterior = getPDFBeta(posterior[0], posterior[1])
    pdf_likelihood = getPDFBinomail(N_H, N)
    plt.plot(x, pdf_prior, color="tab:blue", linewidth=3, label=r"Prior")
    plt.plot(x, pdf_posterior, color="tab:red", linewidth=3, label=r"Posterior")
    # SCALE THE LIKELIHOOD
    max_ = np.max([np.max(pdf_posterior), np.max(pdf_prior)])
    pdf_likelihood = pdf_likelihood * max_ / np.max(pdf_likelihood)
    plt.plot(x, pdf_likelihood, color="tab:green", linewidth=3, label=r"Likelihood")
    N_points = 10
    plt.plot(np.ones(N_points)*P_H, np.linspace(0,1,N_points)*max_, color="tab:orange", alpha=0.6, label="Truth", linewidth=3)
    plt.legend(loc="upper left", bbox_to_anchor=(1.05, 1), borderaxespad=0.)
    plt.tight_layout()
    plt.show()
    

def Bernulli(P_H):
    # Sampling from a Bernulli distribution with parameter P_H
    sample = np.random.rand()
    if sample < P_H:
        return "H"
    else:
        return "T"

def coinToss(N=1):
    result = []
    # number of heads
    N_H = 0
    for run in range(N):
        coin_toss = Bernulli(P_H)
        if coin_toss == "H": N_H += 1
    print("# coinToss() # {:}/{:} heads.".format(N_H, N))
    return N_H



# ---------------------------------------------------
# A strong inaccurate prior belief
# decelerates convergence to an accurate posterior estimate 
# The closer we are to the prior the faster the convergence
# ---------------------------------------------------
# Prior, number of experiments, each of N_exp runs
Prior = [100, 20]
N_exp = 10
N_runs_per_exp = 10

Posterior = []
Posterior.append(Prior)
for i in range(N_exp):
    N = N_runs_per_exp
    N_H = coinToss(N)
    a, b = Prior
    a += N_H
    b += N-N_H
    posterior = [a,b]
    Posterior.append(posterior)

    # Plot one experiment
    plotExperiment(Prior, posterior, N, N_H)

    # Todays posterior is tomorrow's prior !
    Prior = posterior



x = np.linspace(0, 1, 100)
for i in range(len(Posterior)):
    a, b = Posterior[i]
    posterior = beta(a, b)
    pdf = posterior.pdf(x)

    plt.plot(x, pdf, color="tab:blue", linewidth=3, label=r"$\alpha={:}, \beta={:}$".format(a, b), alpha=(i+1)/len(Posterior))
plt.show()








