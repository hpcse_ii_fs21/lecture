import numpy as np
from scipy.stats import beta
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

# Beta distribution with a=b=1 is the uniform distribution 

x = np.linspace(0, 1, 100)

for temp in [[1,1], [4,4], [10,10], [20,20]]:

    # a, b = 1, 1
    a,b = temp
    rv = beta(a, b)
    pdf = rv.pdf(x)

    plt.plot(x, pdf, linewidth=2, label=r"$\alpha={:}, \beta={:}$".format(a, b))

plt.xlabel(r"$P_H$")
plt.ylabel(r"PDF $p(P_H)$")
plt.title(r"$Beta(\alpha, \beta)$")
plt.legend(loc="upper left", bbox_to_anchor=(1.05, 1), borderaxespad=0.)
# plt.ylim([0,1.1])
# plt.tight_layout()
plt.savefig("1_iform_prior.png", dpi=400, bbox_inches='tight')
# plt.show()
plt.close()

