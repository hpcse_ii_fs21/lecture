import numpy as np
from scipy.stats import beta
import matplotlib
import matplotlib.pyplot as plt

# True (unknown) probability of the coin toss coming HEADS
P_H = 0.7

def Bernulli(P_H):
    # Sampling from a Bernulli distribution with parameter P_H
    sample = np.random.rand()
    if sample < P_H:
        return "H"
    else:
        return "T"

def coinToss(N=1):
    result = []
    # number of heads
    N_H = 1
    for run in range(N):
        coin_toss = Bernulli(P_H)
        if coin_toss == "H": N_H += 1
    return N_H


def run_experiment(Prior, N_exp, N_runs_per_exp):
    converged=False
    Posterior = []
    Posterior.append(Prior)
    N_exp_run = 0
    for i in range(N_exp):
        N = N_exp
        N_H = coinToss(N)
        a, b = Prior
        a += N_H
        b += N-N_H
        posterior = [a,b]
        Posterior.append(posterior)
        # Todays posterior is tomorrow's prior !
        Prior = posterior
        N_exp_run +=1

        posterior_mean = beta(a,b).mean()
        if np.abs(posterior_mean-P_H)/P_H < 0.001:
            converged=True
            break

    if converged:
        print("Converged after {:} experiments".format(N_exp_run))
    return N_exp_run

# # Prior, number of experiments, each of N_exp runs
# Prior = [1, 1]
# N_exp = 10
# N_runs_per_exp = 10

# # Prior, number of experiments, each of N_exp runs
# Prior = [1, 100]
# N_exp = 10000
# N_runs_per_exp = 1000

# Prior, number of experiments, each of N_exp runs
Prior = [10, 1]
N_exp = 10000
N_runs_per_exp = 100

# # Prior, number of experiments, each of N_exp runs
# Prior = [10, 10]
# N_exp = 10000
# N_runs_per_exp = 1000


trials = 10
temp = []
for trial in range(trials):
    N_exp_till_conv = run_experiment(Prior, N_exp, N_runs_per_exp)
    temp.append(N_exp_till_conv)

print(temp)
# x = np.linspace(0, 1, 100)
# for i in range(len(Posterior)):
#     a, b = Posterior[i]
#     posterior = beta(a, b)
#     pdf = posterior.pdf(x)

#     plt.plot(x, pdf, color="tab:blue", linewidth=3, label=r"$\alpha={:}, \beta={:}$".format(a, b), alpha=(i+1)/len(Posterior))
# plt.show()









