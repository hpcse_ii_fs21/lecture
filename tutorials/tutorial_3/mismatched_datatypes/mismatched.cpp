#include <mpi.h>
#include <cstdio>

// This small program shows how to send/recv messages of
// dissimilar datatypes (as long as the type signatures coincide)

int main(int argc, char ** argv){
    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // TODO Create Send type which allows sending only the diagonal
    MPI_Datatype diag_t;

    // TODO Create Recv type which allows recv the diagonal in a single contiguous array
    MPI_Datatype recv_arr_t;


    // Send and Recv buffers
    int state_matrix[9];
    int diag_arr[3];

    if(rank == 0) {
        // Initialize with some values
        for(int i = 0; i < 9; ++i) {
            state_matrix[i] = i;
        }

        // TODO Send the diagonal of the 'state_matrix'
        //MPI_Send (...);

    } else {
        // TODO Recv the diagonal in the contiguous buffer 'diag_arr'
        //MPI_Recv (...);

        printf("Received diagonal elements: {%d, %d, %d}\n", diag_arr[0], diag_arr[1], diag_arr[2]);
    }

    MPI_Type_free(&diag_t);
    MPI_Type_free(&recv_arr_t);

    MPI_Finalize();
    return 0;
}
