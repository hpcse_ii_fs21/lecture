#include <mpi.h>
#include <stdio.h>
#include <cmath>

struct Particle
{
  int id;
  double x[2];
  bool state;
  double gamma;
};

void print(Particle p){
    printf("%-7s %d \n", "id:",p.id );
    printf("%-7s %10.8f \n", "p.x:", p.x[0]  );
    printf("%-7s %10.8f \n", "", p.x[1]  );
    printf("%-7s %d \n"    , "state:", p.state );
    printf("%-7s %10.8f \n", "gamma:", p.gamma );
}

int main(int argc, char ** argv)
{
	MPI_Init(&argc, &argv);

  int rank,size;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  // Single Particle instance
  Particle p;

  MPI_Datatype particle_t;
  // TODO create new struct types


  // Send & Recv Particle
  if (rank == 0)
  {
    p.id = 1;
    p.x[0] = 3.14159265359;
    p.x[1] = 2.71828182846;
    p.state = true;
    p.gamma = 0.57721566490;

    MPI_Send(&p, 1, particle_t, 1, 42, MPI_COMM_WORLD);
  }
  else if (rank == 1)
  {
    MPI_Recv(&p, 1, particle_t, 0, 42, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    print(p);
  }
  else
  {
    printf("Run with exactly two ranks!\n");
    int err = 1;
    MPI_Abort(MPI_COMM_WORLD,err);
  }

  // Free the datatype
  MPI_Type_free(&particle_t);

  MPI_Finalize();
  return 0;
}
