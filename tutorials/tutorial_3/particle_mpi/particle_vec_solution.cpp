#include <mpi.h>
#include <stdio.h>
#include <cmath>
#include <vector>


struct Particle
{
  int id;
  double x[2];
  bool state;
  double gamma;
};

void print(Particle &p){
    printf("%-7s %d \n", "id:",p.id );
    printf("%-7s %10.8f \n", "p.x:", p.x[0]  );
    printf("%-7s %10.8f \n", "", p.x[1]  );
    printf("%-7s %d \n"    , "state:", p.state );
    printf("%-7s %10.8f \n", "gamma:", p.gamma );
}

void print(std::vector<Particle> &p_vec){
    for(int i = 0; i < p_vec.size(); ++i){
        printf("p_vec[%d] : \n", i);
        print(p_vec[i]);
        printf("\n");
    }
}

void init_vector(std::vector<Particle> &p_vec){
    for(int i = 0; i < p_vec.size(); ++i){
        p_vec[i].id = (i+1)*1;
        p_vec[i].x[0] = (i+1)*3.14159265359;
        p_vec[i].x[1] = (i+1)*2.71828182846;
        p_vec[i].state = true;
        p_vec[i].gamma = (i+1)*0.57721566490;
    }

}

int main(int argc, char ** argv)
{
	MPI_Init(&argc, &argv);

  int rank,size;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  // Single Particle instance
  Particle p;

  // Multiple Particles stored in a vector
  std::vector<Particle> particle_vec(2);

  MPI_Datatype particle_t, tmp_t;

   /*
    *Partial struct in vector (id, x[2], state, [ ])
    */

  int count = 3;
  int blocklens[] = {1, 2, 1};
  MPI_Datatype types[] = {MPI_INT, MPI_DOUBLE, MPI_C_BOOL};

  // Compute offsets
  MPI_Aint p_lb, p_id, p_x, p_state, p_ub, p_extent;
  MPI_Get_address (&particle_vec[0], &p_lb);
  MPI_Get_address (&particle_vec[0].id, &p_id);
  MPI_Get_address (&particle_vec[0].x[0], &p_x);
  MPI_Get_address (&particle_vec[0].state, &p_state);
  MPI_Get_address (&particle_vec[1], &p_ub);

  MPI_Aint offsets[] = {MPI_Aint_diff(p_id, p_lb),
                        MPI_Aint_diff(p_x, p_lb),
                        MPI_Aint_diff(p_state, p_lb)};

  // Create datatype
  MPI_Type_create_struct (count, blocklens, offsets, types, &tmp_t);

  p_extent = MPI_Aint_diff(p_ub, p_lb);
  MPI_Type_create_resized(tmp_t, p_lb, p_extent, &particle_t);
  MPI_Type_commit (&particle_t);


  // Send & Recv Particle
  if (rank == 0)
  {
    init_vector(particle_vec);

    MPI_Send(&particle_vec[0], 2, particle_t, 1, 42, MPI_COMM_WORLD);
  }
  else
  {
    MPI_Recv(&particle_vec[0], 2, particle_t, 0, 42, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    print(particle_vec);
  }

  // Free the datatype
  MPI_Type_free(&particle_t);

  MPI_Finalize();
  return 0;
}
