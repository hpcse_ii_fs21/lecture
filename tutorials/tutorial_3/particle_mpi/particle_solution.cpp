#include <mpi.h>
#include <stdio.h>
#include <cmath>
#include <vector>


struct Particle
{
  int id;
  double x[2];
  bool state;
  double gamma;
};

void print(Particle p){
    printf("%-7s %d \n", "id:",p.id );
    printf("%-7s %10.8f \n", "p.x:", p.x[0]  );
    printf("%-7s %10.8f \n", "", p.x[1]  );
    printf("%-7s %d \n"    , "state:", p.state );
    printf("%-7s %10.8f \n", "gamma:", p.gamma );
}

int main(int argc, char ** argv)
{
	MPI_Init(&argc, &argv);

  int rank,size;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  // Single Particle instance
  Particle p;

  MPI_Datatype particle_t;


  /*
   *Full struct
   */

  int count = 4;
  int blocklens[] = {1, 2, 1, 1};
  MPI_Datatype types[] = {MPI_INT, MPI_DOUBLE, MPI_C_BOOL, MPI_DOUBLE};

  // Compute offsets
  MPI_Aint p_lb, p_id, p_x, p_state, p_gamma, p_extent;
  MPI_Get_address (&p, &p_lb);
  MPI_Get_address (&p.id, &p_id);
  MPI_Get_address (&p.x[0], &p_x);
  MPI_Get_address (&p.state, &p_state);
  MPI_Get_address (&p.gamma, &p_gamma);
  MPI_Aint offsets[] = {MPI_Aint_diff(p_id, p_lb),
                        MPI_Aint_diff(p_x, p_lb),
                        MPI_Aint_diff(p_state, p_lb),
                        MPI_Aint_diff(p_gamma, p_lb)};

  // Create datatype
  MPI_Type_create_struct (count, blocklens, offsets, types, &particle_t);

  MPI_Type_commit (&particle_t);


   /*
    *Partial struct (id, x[2], [ ], gamma)
    */

/*
 *  int count = 3;
 *  int blocklens[] = {1, 2, 1};
 *  MPI_Datatype types[] = {MPI_INT, MPI_DOUBLE, MPI_DOUBLE};
 *
 *  // Compute offsets
 *  MPI_Aint p_lb, p_id, p_x, p_gamma, p_extent;
 *  MPI_Get_address (&p, &p_lb);
 *  MPI_Get_address (&p.id, &p_id);
 *  MPI_Get_address (&p.x[0], &p_x);
 *  MPI_Get_address (&p.gamma, &p_gamma);
 *  MPI_Aint offsets[] = {MPI_Aint_diff(p_id, p_lb),
 *                        MPI_Aint_diff(p_x, p_lb),
 *                        MPI_Aint_diff(p_gamma, p_lb)};
 *
 *  // Create datatype
 *  MPI_Type_create_struct (count, blocklens, offsets, types, &particle_t);
 *
 *  MPI_Type_commit (&particle_t);
 */


   /*
    *Partial struct (id, x[2], state, [ ])
    */

/*
 *  int count = 3;
 *  int blocklens[] = {1, 2, 1};
 *  MPI_Datatype types[] = {MPI_INT, MPI_DOUBLE, MPI_C_BOOL};
 *
 *  // Compute offsets
 *  MPI_Aint p_lb, p_id, p_x, p_state, p_extent;
 *  MPI_Get_address (&p, &p_lb);
 *  MPI_Get_address (&p.id, &p_id);
 *  MPI_Get_address (&p.x[0], &p_x);
 *  MPI_Get_address (&p.state, &p_state);
 *  MPI_Aint offsets[] = {MPI_Aint_diff(p_id, p_lb),
 *                        MPI_Aint_diff(p_x, p_lb),
 *                        MPI_Aint_diff(p_state, p_lb)};
 *
 *  // Create datatype
 *  MPI_Type_create_struct (count, blocklens, offsets, types, &particle_t);
 *
 *  MPI_Type_commit (&particle_t);
 */

  // Send & Recv Particle
  if (rank == 0)
  {
    p.id = 1;
    p.x[0] = 3.14159265359;
    p.x[1] = 2.71828182846;
    p.state = true;
    p.gamma = 0.57721566490;

    MPI_Send(&p, 1, particle_t, 1, 42, MPI_COMM_WORLD);
  }
  else if (rank == 1)
  {
    MPI_Recv(&p, 1, particle_t, 0, 42, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    print(p);
  }
  else
  {
    printf("Run with exactly two ranks!\n");
    int err = 1;
    MPI_Abort(MPI_COMM_WORLD,err);
  }

  // Free the datatype
  MPI_Type_free(&particle_t);

  MPI_Finalize();
  return 0;
}
