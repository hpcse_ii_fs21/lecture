#include <mpi.h>
#include <stdio.h>
#include <cmath>
#include <vector>


struct Particle
{
  int id;
  double x[2];
  bool state;
  double gamma;
};

void print(Particle &p){
    printf("%-7s %d \n", "id:",p.id );
    printf("%-7s %10.8f \n", "p.x:", p.x[0]  );
    printf("%-7s %10.8f \n", "", p.x[1]  );
    printf("%-7s %d \n"    , "state:", p.state );
    printf("%-7s %10.8f \n", "gamma:", p.gamma );
}

void print(std::vector<Particle> &p_vec){
    for(int i = 0; i < p_vec.size(); ++i){
        printf("p_vec[%d] : \n", i);
        print(p_vec[i]);
        printf("\n");
    }
}

void init_vector(std::vector<Particle> &p_vec){
    for(int i = 0; i < p_vec.size(); ++i){
        p_vec[i].id = (i+1)*1;
        p_vec[i].x[0] = (i+1)*3.14159265359;
        p_vec[i].x[1] = (i+1)*2.71828182846;
        p_vec[i].state = true;
        p_vec[i].gamma = (i+1)*0.57721566490;
    }

}

int main(int argc, char ** argv)
{
	MPI_Init(&argc, &argv);

  int rank,size;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  // Single Particle instance
  Particle p;

  // Multiple Particles stored in a vector
  std::vector<Particle> particle_vec(2);

  MPI_Datatype particle_t, tmp_t;

   /*
    *Partial struct in vector (id, x[2], state, [ ])
    */

  // TODO Initialize function arguments (count, blocklens[], types[])
  //int count = ;
  //int blocklens[] = ;
  //MPI_Datatype types[] = ;

  // TODO Compute offsets
  MPI_Aint p_lb, p_id, p_x, p_state, p_ub, p_extent;

  // Create datatype
  //MPI_Type_create_struct (count, blocklens, offsets, types, &tmp_t);

  // TODO Compute p_extent from p_ub and p_lb and resize datatype


  // TODO uncomment
  //MPI_Type_commit (&particle_t);


  // Send & Recv Particle
  if (rank == 0)
  {
    init_vector(particle_vec);

    // TODO uncomment
    //MPI_Send(&particle_vec[0], 2, particle_t, 1, 42, MPI_COMM_WORLD);
  }
  else
  {
    // TODO uncommnet
    //MPI_Recv(&particle_vec[0], 2, particle_t, 0, 42, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    print(particle_vec);
  }


  // Free the datatype
  MPI_Type_free(&particle_t);

  MPI_Finalize();
  return 0;
}
