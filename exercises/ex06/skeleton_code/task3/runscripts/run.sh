#!/bin/bash

SCRIPTDIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

mkdir -p output
"${SCRIPTDIR}/../build/md_simulation" "$@"
