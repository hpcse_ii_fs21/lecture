#!/usr/bin/env python3

import numpy as np

from common import log, run_simulation
from common import data_rdf  # Target RDF as a numpy array.

def model(s):
    # TODO: Read model parameters from Korali sample.
    alpha = 0
    sigma = 0

    rdf = run_simulation(alpha)
    log(rdf, alpha, sigma)
    assert len(rdf) == len(data_rdf)

    # TODO: Return to Korali the evaluations and the error standard deviations.
    s["Reference Evaluations"] = []
    s["Standard Deviation"] = []


import korali
e = korali.Experiment()

# TODO: Set up the Korali Experiment. For reference, see:
# https://github.com/cselab/korali/blob/master/examples/bayesian.inference/reference/run-tmcmc.py



k = korali.Engine()

# Use this on Piz Daint as a replacement for the Sequential conduit.
k["Conduit"]["Type"] = "Concurrent"
k["Conduit"]["Concurrent Jobs"] = 1

k.run(e)
