#!/usr/bin/env python3
#
# Based on the propagation example from korali:
# https://github.com/cselab/korali/tree/master/examples/propagation

import argparse
import json
import korali
import os

from common import log, run_simulation

def model_propagation(s):
    alpha = s["Parameters"][0]
    sigma = s["Parameters"][1]

    rdf = run_simulation(alpha)
    log(rdf, alpha, sigma)

    s["Evaluations"] = rdf


def propagate(samples_dir, propagation_dir):
    # Uncertainty propagation
    e = korali.Experiment()

    # Model: Evaluation
    e["Problem"]["Type"] = "Propagation"
    e["Problem"]["Execution Model"] = model_propagation

    # Load the data from the sampling
    with open(os.path.join(samples_dir, 'latest')) as f:
        d = json.load(f)

    e["Variables"][0]["Name"] = 'alpha'
    v = [p[0] for p in d["Results"]["Sample Database"]]
    e["Variables"][0]["Precomputed Values"] = v

    e["Variables"][1]["Name"] = "[Sigma]"
    v = [p[1] for p in d["Results"]["Sample Database"]]
    e["Variables"][1]["Precomputed Values"] = v

    e["Solver"]["Type"] = "Executor"
    e["Solver"]["Executions Per Generation"] = 1

    e["Console Output"]["Verbosity"] = "Minimal"
    e["File Output"]["Path"] = propagation_dir
    e["Store Sample Information"] = True

    k = korali.Engine()
    k.run(e)


def main():
    description = \
            "Run first with --action=propagate to rerun the latest generation. " \
            "Then, run with --action=plot to plot the results."
    parser = argparse.ArgumentParser(description=description)
    add = parser.add_argument
    add('--samples-dir', type=str, default='korali_results', help="path to folder with TMCMC output")
    add('--propagation-dir', type=str, default='korali_propagation', help="folder to store output from propagation")
    add('--plot-path', type=str, default=os.path.join('plots', 'propagation.png'),
        help="target path for the plot, if empty plt.show() will be used instead")
    add('--action', choices=('propagate', 'plot'), required=True)
    args = parser.parse_args()

    if args.action == 'propagate':
        propagate(args.samples_dir, args.propagation_dir)
    else:
        from common import data_x, data_rdf
        import sys
        sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'plotting'))
        from plot_credible_intervals import plot_credible_intervals
        plot_credible_intervals(
                os.path.join(args.propagation_dir, 'latest'), data_x, data_rdf,
                args.plot_path)


main()
