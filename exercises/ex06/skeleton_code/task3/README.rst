Task 3: RDF computation and inference of model parameters
=========================================================

Compiling
---------

.. code-block:: bash

    # Starting from the task3/ folder:
    meson setup build
    cd build
    ninja

Running
-------

.. code-block:: bash

    # Starting from the task3/ folder:
    cd runscripts
    ./run.sh

    # Generate RDF plot, particle scatter plot and particle animation.
    ./postprocess.sh

    # Check available flags:
    ./run.sh --help


UQ
--

.. code-block:: bash

    # Starting from the task3/ folder:
    cd runscripts
    ./uq.py

    # Interactive plot (pass `--output filename.png` to plot to file instead):
    python3 -m korali.plotter --dir korali_results

    # Propagation of uncertainity:
    ./propagation.py --action=propagate
    ./propagation.py --action=plot
