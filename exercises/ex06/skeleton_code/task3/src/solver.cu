#include "solver.h"
#include "../../include/utils.h"
#include <random>

Solver::Solver(Interaction interaction,
               double2 domainSize,
               double vStdDev,
               double berendsenTau,
               int numParticles,
               unsigned long long seed) :
    interaction_{interaction},
    domainSize_{domainSize},
    numParticles_{numParticles},
    vStdDev_{vStdDev},
    berendsenTau_{berendsenTau},
    cellList_{domainSize, interaction.getCutoff()}
{
    CUDA_CHECK(cudaMalloc(&pOldDev_, numParticles * sizeof(pOldDev_[0])));
    CUDA_CHECK(cudaMalloc(&vOldDev_, numParticles * sizeof(vOldDev_[0])));
    CUDA_CHECK(cudaMalloc(&pDev_, numParticles * sizeof(pDev_[0])));
    CUDA_CHECK(cudaMalloc(&vDev_, numParticles * sizeof(vDev_[0])));
    CUDA_CHECK(cudaMalloc(&fDev_, numParticles * sizeof(fDev_[0])));

    CUDA_CHECK(cudaMallocHost(&pHost_, numParticles * sizeof(pHost_[0])));
    CUDA_CHECK(cudaMallocHost(&vHost_, numParticles * sizeof(vHost_[0])));
    std::mt19937 gen{seed};
    std::uniform_real_distribution<double> uniform{0.0, 1.0};
    // For some reason, setting stddev=vStdDev causes too high v^2 according to
    // the Berendsen thermostat. To speed up the warmup period, we reduce the
    // initial velocities by a factor of 2.
    std::normal_distribution<double> normal{0.0, 0.5 * vStdDev};
    for (int i = 0; i < numParticles; ++i) {
        pHost_[i].x = uniform(gen) * domainSize_.x;
        pHost_[i].y = uniform(gen) * domainSize_.y;
        vHost_[i].x = normal(gen);
        vHost_[i].y = normal(gen);
    }
    CUDA_CHECK(cudaMemcpy(pDev_, pHost_, numParticles * sizeof(pHost_[0]), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(vDev_, vHost_, numParticles * sizeof(vHost_[0]), cudaMemcpyHostToDevice));

    // Keep the number of blocks used for reduction a multiple of warpSize (to
    // simplify the implementation of the 2nd stage) and at most 1024 (such
    // that in 1 2nd stage thread == 1 1st stage block). We hard-code the
    // number of threads to 1024, see below.
    numReductionBlocks_ = std::min(
            1024,
            (((numParticles + 9) / 10 + 1024 - 1) / 1024 + 31) & ~31);
    // The extra element is for Berendsen lambda factor.
    CUDA_CHECK(cudaMallocHost(&vReductionBuffer_, (numReductionBlocks_ + 1) * sizeof(vReductionBuffer_[0])));
}


Solver::~Solver() {
    CUDA_CHECK(cudaFreeHost(vHost_));
    CUDA_CHECK(cudaFreeHost(pHost_));
    CUDA_CHECK(cudaFree(fDev_));
    CUDA_CHECK(cudaFree(vDev_));
    CUDA_CHECK(cudaFree(pDev_));
    CUDA_CHECK(cudaFree(vOldDev_));
    CUDA_CHECK(cudaFree(pOldDev_));
}


__device__ void bounceBack(double2 domainSize, double2 *p, double2 *v) {
    if (p->x < 0) {
        p->x = -p->x;
        v->x = -v->x;
    } else if (p->x > domainSize.x) {
        p->x = 2 * domainSize.x - p->x;
        v->x = -v->x;
    }
    if (p->y < 0) {
        p->y = -p->y;
        v->y = -v->y;
    } else if (p->y > domainSize.y) {
        p->y = 2 * domainSize.y - p->y;
        v->y = -v->y;
    }
}

__global__ void minimizeKernel(
        double2 domainSize,
        double2 * __restrict__ p,
        double2 * __restrict__ v,
        const double2 * __restrict__ f,
        int N,
        double dt,
        double maxMove) {
    const int pid = blockIdx.x * blockDim.x + threadIdx.x;
    if (pid < N) {
        double2 dr;
        dr.x = dt * dt * f[pid].x;
        dr.y = dt * dt * f[pid].y;
        const double dr2 = dr.x * dr.x + dr.y * dr.y;
        if (dr2 > maxMove * maxMove) {
            const double factor = maxMove * rsqrt(dr2);
            dr.x *= factor;
            dr.y *= factor;
        }
        p[pid].x += dr.x;
        p[pid].y += dr.y;

        bounceBack(domainSize, &p[pid], &v[pid]);
    }
}

__global__ void vvKernel(
        double2 domainSize,
        double2 * __restrict__ p,
        double2 * __restrict__ v,
        const double2 * __restrict__ f,
        int N,
        double dt) {
    const int pid = blockIdx.x * blockDim.x + threadIdx.x;
    if (pid < N) {
        v[pid].x += dt * f[pid].x;
        v[pid].y += dt * f[pid].y;
        p[pid].x += dt * v[pid].x;
        p[pid].y += dt * v[pid].y;

        bounceBack(domainSize, &p[pid], &v[pid]);
    }
}

void Solver::timestep(double dt, double maxMinimizationMove) {
    std::swap(pDev_, pOldDev_);
    std::swap(vDev_, vOldDev_);
    cellList_.build(pOldDev_, vOldDev_, pDev_, vDev_, numParticles_);
    computeForces(cellList_.getInfo(), interaction_, pDev_, fDev_, numParticles_);

    const int threads = 256;
    const int blocks = (numParticles_ + threads - 1) / threads;
    if (maxMinimizationMove > 0) {
        minimizeKernel<<<blocks, threads>>>(
                domainSize_, pDev_, vDev_, fDev_, numParticles_, dt, maxMinimizationMove);
    } else {
        vvKernel<<<blocks, threads>>>(
                domainSize_, pDev_, vDev_, fDev_, numParticles_, dt);
        _berendsenThermostat(dt);
    }
}

void Solver::dumpParticlesCSV(const char *path) {
    CUDA_CHECK(cudaMemcpy(pHost_, pDev_, numParticles_ * sizeof(pHost_[0]), cudaMemcpyDeviceToHost));
    CUDA_CHECK(cudaMemcpy(vHost_, vDev_, numParticles_ * sizeof(vHost_[0]), cudaMemcpyDeviceToHost));

    FILE * const f = fopen(path, "w");
    if (f == nullptr) {
        fprintf(stderr, "Error opening \"%s\" in write mode, maybe the folder is missing?\n", path);
        exit(3);
    }
    fprintf(f, "x,y,vx,vy\n");
    for (int i = 0; i < numParticles_; ++i)
        fprintf(f, "%g,%g,%g,%g\n", pHost_[i].x, pHost_[i].y, vHost_[i].x, vHost_[i].y);
    fclose(f);
}

/// Compute the sum of 32 numbers of a warp. The function assumes all 32 lanes are active.
__device__ double warpSum(double x) {
    x += __shfl_down_sync(0xFFFFFFFF, x, 16);
    x += __shfl_down_sync(0xFFFFFFFF, x, 8);
    x += __shfl_down_sync(0xFFFFFFFF, x, 4);
    x += __shfl_down_sync(0xFFFFFFFF, x, 2);
    x += __shfl_down_sync(0xFFFFFFFF, x, 1);
    return x;
}

/// Compute block sums and store them in the given output buffer.
__global__ void vReductionKernel1(const double2 *v, double *blockSums, int numParticles) {
    double sum = 0.0;
    for (int i = blockIdx.x * blockDim.x + threadIdx.x;
             i < numParticles;
             i += gridDim.x * blockDim.x) {
        sum += v[i].x * v[i].x + v[i].y * v[i].y;
    }

    // We have 1024 = 32x32 threads per block. That means each warp can compute
    // its sum and store it in a temporary shared array. Then, the first warp
    // reduces these 32 warp sums into 1 block sum.
    const double _warpSum = warpSum(sum);
    __shared__ double warpSums[32];
    if ((threadIdx.x & 31) == 0)
        warpSums[threadIdx.x >> 5] = _warpSum;
    __syncthreads();
    if (threadIdx.x < 32) {
        const double blockSum = warpSum(warpSums[threadIdx.x]);
        if (threadIdx.x == 0)
            blockSums[blockIdx.x] = blockSum;
    }
}

/// Reduce the block sums to compute the total sum of v^2.
/// Immediately compute the Berendsen lambda factor.
__global__ void vReductionKernel2(
        const double *blockSums,
        int numParticles,
        double dtOverTau,
        double vStdDev,
        double *lambda) {
    __shared__ double warpSums[32];
    if (threadIdx.x < 32)
        warpSums[threadIdx.x] = 0.0;
    __syncthreads();
    const double _warpSum = warpSum(blockSums[threadIdx.x]);
    if ((threadIdx.x & 31) == 0)
        warpSums[threadIdx.x >> 5] = _warpSum;
    __syncthreads();

    if (threadIdx.x >= 32)
        return;
    const double totalVelSqr = warpSum(warpSums[threadIdx.x]);
    if (threadIdx.x > 0)
        return;

    const double avgVelSqr = totalVelSqr / numParticles;
    const double targetVelSqr = vStdDev * vStdDev;
    *lambda = std::sqrt(1 + dtOverTau * (targetVelSqr / (1e-5 * targetVelSqr + avgVelSqr) - 1));
    // printf("vStdDev=%g avgVel=%g lambda=%g\n", vStdDev, std::sqrt(avgVelSqr), *lambda);
}

__global__ void scaleVelocitiesKernel(double2 *v, const double *lambda, int numParticles) {
    int pid = blockIdx.x * blockDim.x + threadIdx.x;
    if (pid < numParticles) {
        double l = *lambda;
        v[pid].x *= l;
        v[pid].y *= l;
    }
}

void Solver::_berendsenThermostat(double dt) {
    double * const lambdaDev = &vReductionBuffer_[numReductionBlocks_];
    vReductionKernel1<<<numReductionBlocks_, 1024>>>(
            vDev_, vReductionBuffer_, numParticles_);
    vReductionKernel2<<<1, numReductionBlocks_>>>(
            vReductionBuffer_, numParticles_, dt / berendsenTau_, vStdDev_, lambdaDev);
    scaleVelocitiesKernel<<<(numParticles_ + 1024 - 1) / 1024, 1024>>>(vDev_, lambdaDev, numParticles_);
}
