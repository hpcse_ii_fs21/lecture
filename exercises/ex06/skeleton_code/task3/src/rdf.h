#pragma once

#include "cell_list.h"

class RDF {
public:
    /** Create an RDF object.

        Since the non-periodic bounce-back boundary affects the distribution of
        particles near the domain boundary, we do not take near-boundary
        particles as reference particles when computing the RDF. Concretely, we
        skip particles whose distance to the boundary is less than `margin`.
    */
    RDF(double2 domainSize, double margin, double binSize, int numBins);
    ~RDF();

    /// Increase the RDF histogram with new samples.
    void updateRDF(
            const CellList &cellList,
            const double2 *p,
            int numParticles);

    /// Dump RDF to the given path as a CSV file with columns.
    /// If path is equal to "stdout", stdout is used instead.
    void dumpRDF(const char *path, bool quiet);

    /// For testing...
    const int *_getBinsDev() const { return binsDev_; }

private:
    const double2 low_;   ///< Lower suitable coordinate.
    const double2 high_;  ///< Highest suitable coordinate.
    const double binSize_;
    const int numBins_;

    // The bin counts containers. Both container have (numBins_ + 1) elements,
    // where the last extra element is used to count the total number of
    // reference points (samples).
    int *binsDev_ = nullptr;
    int *binsHost_ = nullptr;
    int numUpdates_ = 0;
};
