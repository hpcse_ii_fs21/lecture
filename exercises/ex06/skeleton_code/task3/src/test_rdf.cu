#include "cell_list.h"
#include "rdf.h"
#include "../../include/utils.h"
#include <vector>

static void testRDF(
        const char *testName,
        double2 domainSize,
        double cellSize,
        double margin,
        double binSize,
        std::vector<double2> pos,
        std::vector<int> expectedBinCounts) {
    const int N = (int)pos.size();
    const int K = (int)expectedBinCounts.size() - 1;

    double2 *pDev;
    double2 *vDev;
    double2 *pSortedDev;
    double2 *vSortedDev;
    int *binsHost;
    CUDA_CHECK(cudaMalloc(&pDev, N * sizeof(double2)));
    CUDA_CHECK(cudaMalloc(&vDev, N * sizeof(double2)));
    CUDA_CHECK(cudaMalloc(&pSortedDev, N * sizeof(double2)));
    CUDA_CHECK(cudaMalloc(&vSortedDev, N * sizeof(double2)));
    CUDA_CHECK(cudaMemcpy(pDev, pos.data(), N * sizeof(double2), cudaMemcpyHostToDevice));

    // Extra element for the total number of samples.
    CUDA_CHECK(cudaMallocHost(&binsHost, (K + 1) * sizeof(int)));

    CellList cellList{domainSize, cellSize};
    cellList.build(pDev, vDev, pSortedDev, vSortedDev, N);

    RDF rdf{domainSize, margin, binSize, K};

    bool error = false;
    int iter = 1;
    for (; !error && iter <= 2; ++iter) {
        rdf.updateRDF(cellList, pSortedDev, N);
        CUDA_CHECK(cudaMemcpy(binsHost, rdf._getBinsDev(),
                              (K + 1) * sizeof(int), cudaMemcpyDeviceToHost));

        for (int i = 0; i <= K; ++i) {
            if (binsHost[i] != iter * expectedBinCounts[i]) {
                error = true;
                break;
            }
        }
        if (error)
            break;
    }

    if (error) {
        printf("TEST FAILED: %s\n", testName);
        printf("domain=(%g %g)  cellSize=%g  margin=%g  binSize=%g  numBins=%d\n",
               domainSize.x, domainSize.y, cellSize, margin, binSize, K);
        printf("Positions (N=%d):\n", N);
        for (int i = 0; i < N; ++i)
            printf("%d %g %g\n", i, pos[i].x, pos[i].y);
        printf("Bins:\n");
        printf("Expected  Computed\n");
        for (int i = 0; i <= K; ++i) {
            if (i == K) {
                printf("Total samples:\n");
                printf("Expected  Computed\n");
            }
            printf("%8d  %8d%s\n",
                   expectedBinCounts[i], binsHost[i],
                   expectedBinCounts[i] != binsHost[i] ? " <------" : "");
        }
        if (iter > 1) {
            printf("Test failed in iteration #%d! "
                   "Are you sure the RDF function is incrementing "
                   "the bin counts and not resetting them?\n", iter);
        }
    }

    CUDA_CHECK(cudaFreeHost(binsHost));
    CUDA_CHECK(cudaFree(vSortedDev));
    CUDA_CHECK(cudaFree(pSortedDev));
    CUDA_CHECK(cudaFree(vDev));
    CUDA_CHECK(cudaFree(pDev));

    if (error) {
        exit(1);
    } else {
        printf("TEST PASSED: %s\n", testName);
    }
}


static void runSmallRDFTests() {
    testRDF("2 particles, no margin",
            {10.0, 10.0}, 1.0, 0.0, 0.1,
            {
                {5.0, 5.0},
                {5.11, 5.0},
            },
            {0, 2, 0, 0, 0, 0, 0, 0, 0, 2});

    testRDF("3 particles, no margin",
            {10.0, 10.0}, 1.0, 0.0, 0.1,
            {
                {5.0, 5.0},
                {5.11, 5.0},
                {5.48, 5.0},
            },
            {0, 2, 0, 2, 2, 0, 0, 0, 0, 3});

    testRDF("2 particles, no margin, span larger than r_c",
            {10.0, 10.0}, 1.0, 0.0, 0.1,
            {
                {5.0, 5.0},
                {6.81, 5.0},
            },
            {0, 0, 0, 0, 0,  0, 0, 0, 0, 0,  0, 0, 0, 0, 0,  0, 0, 0, 2, 0, 2});

    testRDF("2 particles, margin",
            {10.0, 10.0}, 1.0, 3.0, 0.1,
            {
                {2.99, 5.0},  // skipped as reference, but counted otherwise
                {3.01, 5.0},
            },
            {1, 0, 0, 1});

    testRDF("4 pairs, testing all four boundaries",
            {10.0, 15.0}, 1.0, 3.0, 0.1,
            {
                {2.99, 5.0},  // skipped as reference, but counted otherwise
                {3.01, 5.0},
                {7.01, 5.0},  // skipped as reference, but counted otherwise
                {6.99, 5.0},
                {5.0, 12.01},  // skipped as reference, but counted otherwise
                {5.0, 11.99},
                {5.0, 2.99},  // skipped as reference, but counted otherwise
                {5.0, 3.01},
            },
        {4, 0, 0, 4});

}

/*
#include <random>

static void runRandomRDFTest() {
    std::mt19937 gen;
    std::uniform_real_distribution<double> distr{0.0, 5.0};
    std::vector<double2> p(40);
    for (size_t i = 0; i < p.size(); ++i) {
        p[i].x = distr(gen);
        p[i].y = distr(gen);
    }
    testRDF("random test",
            {5.0, 5.0}, 1.0, 0.8, 0.1,
            std::move(p),
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
}
*/

static void runRandomRDFTest() {
    std::vector<double2> p{
        {0.677385, 4.17504},
        {4.84434, 1.10517},
        {1.54084, 2.7361},
        {0.94191, 4.96441},
        {4.98231, 4.83847},
        {3.62919, 4.90555},
        {0.549309, 3.99053},
        {1.48515, 0.0239174},
        {0.562323, 3.19882},
        {4.39215, 2.51831},
        {3.98964, 1.80647},
        {1.05962, 3.4068},
        {1.99369, 3.70324},
        {2.37379, 2.11044},
        {0.869326, 1.50957},
        {3.9864, 1.58275},
        {4.36214, 0.74557},
        {4.97034, 4.10952},
        {0.625914, 3.81875},
        {2.45295, 3.31803},
        {0.629483, 1.05105},
        {0.256082, 0.182206},
        {2.04366, 2.28995},
        {2.43784, 3.96987},
        {4.60437, 4.03766},
        {3.52887, 0.0140922},
        {3.55352, 3.2198},
        {2.28016, 3.86959},
        {2.86877, 4.38379},
        {4.04088, 0.0888695},
        {4.10623, 4.1042},
        {4.70037, 2.06333},
        {2.11583, 2.90478},
        {0.790288, 3.80866},
        {1.15078, 4.04867},
        {4.94261, 1.66224},
        {1.49916, 0.0676956},
        {1.08619, 4.53682},
        {4.24234, 4.77509},
        {3.89449, 4.9373},
    };
    testRDF("random test",
            {5.0, 5.0}, 1.0, 0.8, 0.1,
            std::move(p),
            {0, 2, 2, 4, 4, 16, 10, 3, 11, 12, 12, 16, 7, 18, 24, 10, 15});
}

void runRDFTests() {
    runSmallRDFTests();
    runRandomRDFTest();
}
