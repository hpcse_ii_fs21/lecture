#include "cell_list.h"
#include "interaction.h"
#include "../../include/utils.h"
#include <algorithm>

CellList::CellList(double2 domainSize, double cellSize) {
    numCells_.x = std::max(1, (int)std::floor(domainSize.x / cellSize));
    numCells_.y = std::max(1, (int)std::floor(domainSize.y / cellSize));
    invCellSize_.x = numCells_.x / domainSize.x;
    invCellSize_.y = numCells_.y / domainSize.y;

    // TODO: Copy from Task 2.
}

CellList::~CellList() {
    // TODO: Copy from Task 2.
}

void CellList::build(const double2 *pDev, const double2 *vDev,
                     double2 *pSortedDev, double2 *vSortedDev, int numParticles) {

    // For preventing unused variable warnings... Remove.
    (void)pDev;
    (void)vDev;
    (void)pSortedDev;
    (void)vSortedDev;
    (void)numParticles;

    // TODO: Copy the code from Task 2 and extend to work with velocities too.
}

CellListInfo CellList::getInfo() const {
    return {invCellSize_, numCells_, offsetsDev_};
}
