#include "rdf.h"
#include "../../include/utils.h"
#include <cmath>
#include <cstdio>
#include <cstring>

RDF::RDF(double2 domainSize, double margin, double binSize, int numBins) :
    low_{margin, margin},
    high_{domainSize.x - margin, domainSize.y - margin},
    binSize_{binSize},
    numBins_{numBins}
{
    // Allocate the bins. To avoid having a separate buffer only for the total
    // number of samples (total number of reference points), we use b[K] as the
    // total sample counter.

    // One extra bin used for the total number of samples.
    CUDA_CHECK(cudaMalloc(&binsDev_, (numBins + 1) * sizeof(binsDev_[0])));
    CUDA_CHECK(cudaMallocHost(&binsHost_, (numBins + 1) * sizeof(binsHost_[0])));

    // Reset all initial values to 0, important!
    CUDA_CHECK(cudaMemset(binsDev_, 0, (numBins + 1) * sizeof(binsDev_[0])));
}

RDF::~RDF() {
    CUDA_CHECK(cudaFreeHost(binsHost_));
    CUDA_CHECK(cudaFree(binsDev_));
}

__global__ void updateRDFKernel(
    // TODO: Add all parameters you will need.

        ) {

    // TODO: (b) Implement the kernel: checking if the current particle is
    // suitable reference particle, incresing the total samples counter
    // (binsDev[numBins]), iterate over nearby particles using cell lists,
    // increasing corresponding binsDev[k] counts.

    // TODO: (c) Make a temporary replacement array for binsDev in the shared
    // memory, and operate first there. After every thread completed counting,
    // add the accumulated counts to the global binsDev. Assume that numBins is
    // sufficiently small for the shared array to fit into the shared memory
    // (e.g. numBins < 1000).

}

void RDF::updateRDF(
        const CellList &cellList,
        const double2 *p,
        int numParticles) {
    (void)cellList;
    (void)p;
    (void)numParticles;

    // TODO: Launch the kernel.

    ++numUpdates_;
}

void RDF::dumpRDF(const char *path, bool quiet) {
    CUDA_CHECK(cudaMemcpy(binsHost_, binsDev_, (numBins_ + 1) * sizeof(binsDev_[0]),
                          cudaMemcpyDeviceToHost));

    const int totalSamples = binsHost_[numBins_];
    const double area = (high_.x - low_.x) * (high_.y - low_.y);
    const double numberDensity = totalSamples / (numUpdates_ * area);

    if (!quiet) {
        printf("Saving RDF to \"%s\". Collected %d samples in %d updates.\n",
               path, totalSamples, numUpdates_);
        // This number density will not match exactly the density in main.cu
        // because of the boundary effects of the non-periodic bounce-back
        // domain.
        // printf("Estimated number density: %g / unit square\n", numberDensity);
    }

    const bool isStdout = strcmp(path, "stdout") == 0;
    FILE *f;
    if (isStdout) {
        f = stdout;
    } else if ((f = fopen(path, "w")) == nullptr) {
        fprintf(stderr, "Error opening file \"%s\" in write mode, maybe the folder is missing?\n",
                path);
        exit(1);
    }

    fprintf(f, "r,count,rdf\n");
    for (int i = 0; i < numBins_; ++i) {
        const double r0 = i * binSize_;
        const double r1 = (i + 1) * binSize_;
        const double binArea = M_PI * (r1 * r1 - r0 * r0);
        const double rdf = binsHost_[i] / (binArea * numberDensity * totalSamples);
        fprintf(f, "%g,%d,%g\n", r0, binsHost_[i], rdf);
    }

    if (!isStdout)
        fclose(f);
}
