#include "cell_list.h"
#include "interaction.h"
#include "rdf.h"
#include "solver.h"
#include <cmath>
#include <cstdio>
#include <cstring>

void runRDFTests();  // test_rdf.cu

struct SimulationParams {
    Interaction interaction{1.0, 0.2};
    double2 domainSize;
    double vStdDev;       ///< Effective temperature.
    double dt;            ///< Time step.
    double berendsenTau;  ///< Time scale of the thermostat.
    unsigned long long seed = 12345;
    int numParticles;
    int numMinimizationTimesteps;
    int numWarmupTimesteps;
    int numTimesteps;
    double rdfMargin = 5.0;
    double rdfBinSize = 0.05;
    int rdfNumBins = 40;
    int rdfEvery;
    int dumpEvery;
};

static void simulate(SimulationParams sp, bool quiet, const char *rdfPath) {
    Solver solver{sp.interaction, sp.domainSize, sp.vStdDev, sp.berendsenTau,
                  sp.numParticles, sp.seed};

    if (!quiet) {
        printf("Running a simulation with %d particles on a %gx%g "
               "domain with cutoff %g and interaction alpha %g.\n",
               sp.numParticles, sp.domainSize.x, sp.domainSize.y,
               sp.interaction.getCutoff(), sp.interaction.getAlpha());
        printf("Performing %d minimization timesteps...\n", sp.numMinimizationTimesteps);
    }
    for (int i = 0; i < sp.numMinimizationTimesteps; ++i) {
        // Exponentially increase the "time step" of the minimization from 1e-10 to 1.
        const double progress = (double)(i + 1) / sp.numMinimizationTimesteps;
        const double dt = std::pow(10, -5.0 * (1 - progress)) * sp.dt;
        solver.timestep(sp.dt, 0.01 * sp.interaction.getCutoff());
    }

    if (!quiet)
        printf("Performing %d warmup timesteps...\n", sp.numWarmupTimesteps);
    for (int i = 0; i < sp.numWarmupTimesteps; ++i)
        solver.timestep(sp.dt);

    if (!quiet)
        printf("Performing %d production timesteps...\n", sp.numTimesteps);
    int dumpCount = 0;
    RDF rdf{sp.domainSize, sp.rdfMargin, sp.rdfBinSize, sp.rdfNumBins};
    for (int i = 0; i < sp.numTimesteps; ++i) {
        // Integrate the system equations.
        solver.timestep(sp.dt);

        // Dump particle state if necessary.
        if (sp.dumpEvery > 0 && (i + 1) % sp.dumpEvery == 0) {
            char path[32];
            const int ret = snprintf(
                    path, sizeof(path), "output/particles-%03d.csv", dumpCount);
            if (ret < 0 || ret >= (int)sizeof(path)) {
                fprintf(stderr, "snprintf error ret=%d\n", ret);
                exit(2);
            }
            solver.dumpParticlesCSV(path);
            ++dumpCount;
        }

        // Update RDF if necessary.
        if ((i + 1) % sp.rdfEvery == 0) {
            rdf.updateRDF(solver.getCellList(),
                          solver.getParticlePositions(),
                          sp.numParticles);
        }
    }
    rdf.dumpRDF(rdfPath, quiet);
}

struct CmdlineArgs {
    double alpha = 0.2;
    double domain = 30.0;
    bool quiet = false;
    bool testRDF = false;
    const char *rdfPath = "output/rdf.csv";
    unsigned long long seed = 12345;
};

static void run(const CmdlineArgs &args) {
    // NOTE: Feel free to change the parameters during development, but make
    // sure you have the original setup when running UQ!
    const double cutoff = 1.0;
    const double density = 10.0;
    SimulationParams sp;
    sp.interaction = Interaction{cutoff, args.alpha};
    sp.domainSize = {args.domain, args.domain};
    sp.vStdDev = 1.0;
    sp.dt = 0.01;
    sp.berendsenTau = 100 * sp.dt;
    sp.seed = args.seed;
    sp.numParticles = density * sp.domainSize.x * sp.domainSize.y;
    sp.numMinimizationTimesteps = 100;
    sp.numWarmupTimesteps = 200;
    sp.numTimesteps = 1000;
    sp.rdfEvery = 10;
    sp.dumpEvery = !args.quiet ? 10 : 0;
    simulate(sp, args.quiet, args.rdfPath);
}

static void printUsage(const char *path) {
    fprintf(stderr,
            "Usage: %s [--help] [--quiet] [--alpha <alpha>] [--domain <domain>]\n"
            "       %*s [--rdf-path <rdf-path>] [--seed seed] [--test-rdf]\n",
            path, (int)strlen(path), "");
}

static void printHelp(const char *path) {
    printUsage(path);
    fprintf(stderr,
            "\n"
            "Default --alpha is 0.2.\n"
            "Default --rdf-path is \"output/rdf.csv\"\n"
            "If --rdf-path is set to \"stdout\", stdout will be used.\n"
            "Note that even with a fixed --seed, the result will be stochastic.\n");
}

static int parseArgs(int argc, const char * const *argv, CmdlineArgs *args) {
    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "--help") == 0) {
            printHelp(argv[0]);
            return -1;
        }
        if (strcmp(argv[i], "--alpha") == 0) {
            if (++i >= argc) {
                fprintf(stderr, "Missing value for --alpha.\n");
                return 1;
            }
            if (1 != sscanf(argv[i], "%lf", &args->alpha)) {
                fprintf(stderr, "Expected a floating point number, got \"%s\".\n", argv[i]);
                return 1;
            }
            continue;
        }
        if (strcmp(argv[i], "--domain") == 0) {
            if (++i >= argc) {
                fprintf(stderr, "Missing value for --domain.\n");
                return 1;
            }
            if (1 != sscanf(argv[i], "%lf", &args->domain)) {
                fprintf(stderr, "Expected a floating point number, got \"%s\".\n", argv[i]);
                return 1;
            }
            continue;
        }
        if (strcmp(argv[i], "--quiet") == 0) {
            args->quiet = true;
            continue;
        }
        if (strcmp(argv[i], "--rdf-path") == 0) {
            if (++i >= argc) {
                fprintf(stderr, "Missing value for --rdf-path.\n");
                return 1;
            }
            args->rdfPath = argv[i];
            continue;
        }
        if (strcmp(argv[i], "--seed") == 0) {
            if (++i >= argc) {
                fprintf(stderr, "Missing value for --seed.\n");
                return 1;
            }
            if (1 != sscanf(argv[i], "%llu", &args->seed)) {
                fprintf(stderr, "Expected an unsigned integer, got \"%s\".\n", argv[i]);
                return 1;
            }
            continue;
        }
        if (strcmp(argv[i], "--test-rdf") == 0) {
            args->testRDF = true;
            continue;
        }
        fprintf(stderr, "Unrecognized flag \"%s\".\n", argv[i]);
        printUsage(argv[0]);
        return 1;
    }

    if (args->domain <= 0.0) {
        fprintf(stderr, "--domain should be larger than 0.\n");
        return 1;
    }
    if (!(0.01 <= args->alpha && args->alpha <= 10.0)) {
        fprintf(stderr,
                "alpha should be between 0.01 and 10.0, got %g.",
                args->alpha);
        return 1;
    }

    return 0;
}

int main(int argc, char **argv) {
    CmdlineArgs args;
    int code = parseArgs(argc, argv, &args);
    if (code)
        return code > 0 ? code : 0;
    if (args.testRDF)
        runRDFTests();
    else
        run(args);
}
