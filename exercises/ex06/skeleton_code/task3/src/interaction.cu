#include "cell_list.h"
#include "interaction.h"

void computeForces(
        const CellListInfo info,
        Interaction interaction,
        const double2 *pSortedDev,
        double2 *f,
        int numParticles) {

    // TODO: Copy from Task 2.

    (void)info;
    (void)interaction;
    (void)pSortedDev;
    (void)f;
    (void)numParticles;
}
