import matplotlib.colors
import matplotlib.pyplot as plt
import pathlib

class Plot:
    """Helper class for plotting, used with the `with` statement.

    with Plot(path=...) as (fig, ax):
        ax.plot(...)
        ...

    # On exit, savefig is automatically called.
    """
    def __init__(self, path):
        self.path = path
        self.fig = None
        self.ax = None

    def __enter__(self):
        ratio = 0.8
        self.fig, self.ax = ret = plt.subplots(figsize=(6.4 * ratio, 4.8 * ratio))
        # self.fig, self.ax = ret = plt.subplots()
        return ret

    def __exit__(self, type, value, traceback):
        if type is None:
            if self.path:
                pathlib.Path(self.path).parent.mkdir(parents=True, exist_ok=True)
                self.fig.savefig(self.path, bbox_inches='tight')
                plt.close(self.fig)
                print(f"Plot saved to {self.path}.")
            else:
                print(f"Showing plot...")
                plt.show()
