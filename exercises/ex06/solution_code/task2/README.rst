Task 2: Cell Lists
------------------

Compile and run the code as follows:

.. code-block::

    # Compile.
    meson setup build
    cd build
    ninja

    # Run tests and benchmark.
    ./cell_lists
