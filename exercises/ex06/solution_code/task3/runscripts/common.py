import numpy as np
import os
import subprocess

DATA_PATH = os.path.join(os.path.dirname(__file__), 'target_rdf.csv')
data = np.loadtxt(DATA_PATH, skiprows=1, delimiter=',')
data_x = data[:, 0]
data_rdf = data[:, -1]

counter = 0

def run_simulation(alpha):
    """Run the simulation for the given alpha and return the RDF as a list of floats."""
    cmd = [
        os.path.join(os.path.dirname(__file__), '..', 'build', 'md_simulation'),
        '--alpha', str(alpha),
        '--quiet',
        '--rdf-path', 'stdout',
        '--seed', str(abs(hash((counter, alpha))) % (1 << 63)),
    ]

    result = subprocess.run(cmd, stdout=subprocess.PIPE)
    if result.returncode:
        rdf = [0] * len(data_rdf)
    else:
        csv = result.stdout.decode('utf8').split()
        rdf = [float(line.split(',')[2]) for line in csv[1:]]

    return rdf


def log(rdf, alpha, sigma):
    global counter
    counter += 1
    error = ((np.array(rdf) - data_rdf) ** 2).sum()
    print(f"{counter} alpha={alpha:.5f} sigma={sigma:.5f} --> RDF L2={error}", flush=True)
