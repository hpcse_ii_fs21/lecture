#!/usr/bin/env python3

import numpy as np

from common import log, run_simulation
from common import data_rdf  # Target RDF as a numpy array.

def model(s):
    alpha = s["Parameters"][0]
    sigma = s["Parameters"][1]

    rdf = run_simulation(alpha)
    log(rdf, alpha, sigma)
    assert len(rdf) == len(data_rdf)

    s["Reference Evaluations"] = rdf
    s["Standard Deviation"] = [sigma] * len(data_rdf)


# Creating new experiment
import korali
e = korali.Experiment()

# Setting up the reference likelihood for the Bayesian Problem
e["Problem"]["Type"] = "Bayesian/Reference"
e["Problem"]["Likelihood Model"] = "Normal"
e["Problem"]["Reference Data"] = data_rdf.tolist()
e["Problem"]["Computational Model"] = model

# Configuring TMCMC parameters
e["Solver"]["Type"] = "Sampler/TMCMC"
e["Solver"]["Population Size"] = 800
e["Solver"]["Target Coefficient Of Variation"] = 0.7
e["Solver"]["Covariance Scaling"] = 0.04

# Configuring the problem's random distributions
e["Distributions"][0]["Name"] = "Uniform alpha"
e["Distributions"][0]["Type"] = "Univariate/Uniform"
e["Distributions"][0]["Minimum"] = 0.01
e["Distributions"][0]["Maximum"] = 10.0

e["Distributions"][1]["Name"] = "Uniform [sigma]"
e["Distributions"][1]["Type"] = "Univariate/Uniform"
e["Distributions"][1]["Minimum"] = 0.0
e["Distributions"][1]["Maximum"] = 0.1

# Configuring the problem's variables
e["Variables"][0]["Name"] = "alpha"
e["Variables"][0]["Prior Distribution"] = "Uniform alpha"

e["Variables"][1]["Name"] = "[Sigma]"
e["Variables"][1]["Prior Distribution"] = "Uniform [sigma]"

e["Store Sample Information"] = True

# Configuring output settings
e["File Output"]["Path"] = 'korali_results'
e["File Output"]["Frequency"] = 1
e["Console Output"]["Frequency"] = 1

# Starting Korali's Engine and running experiment
k = korali.Engine()

# Use this on Piz Daint as a replacement for the Sequential conduit.
k["Conduit"]["Type"] = "Concurrent"
k["Conduit"]["Concurrent Jobs"] = 1

k.run(e)
