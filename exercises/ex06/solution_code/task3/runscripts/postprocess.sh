#!/bin/bash

mkdir -p plots
../plotting/plot_rdf.py --input output/rdf.csv --output plots/rdf.png
# ../plotting/plot_particles.py --input output/particles-000.csv --output plots/particles-000.png
../plotting/plot_animation.py --input-glob 'output/particles-*.csv' --output-dir plots/
