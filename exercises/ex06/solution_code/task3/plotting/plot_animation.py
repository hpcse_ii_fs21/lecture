#!/usr/bin/env python

import glob
import multiprocessing
import os

from plot_particles import plot_particles


def make_movie(input_path_format, output_path, pix_fmt='yuv420p'):
    import subprocess

    cmd = [
        'ffmpeg',
        '-stats',
        '-framerate', '10',
        '-i', input_path_format,
        '-b:v', '1M',
        '-c:v', 'libx264',
        '-r', '10',
        '-vf', 'pad=ceil(iw/2)*2:ceil(ih/2)*2',
        '-pix_fmt', pix_fmt,
        '-y', output_path,
        '-loglevel', 'panic',
    ]
    import shlex
    print(' '.join(shlex.quote(arg) for arg in cmd))
    try:
        subprocess.check_call(cmd)
    except:
        print("\nError occurred while running ffmpeg! Try running the command manually and remove the `-loglevel panic` arguments.")
        pass


def plot_particles_animation(input_glob, output_dir, jobs=12):
    input_paths = sorted(glob.glob(input_glob))

    with multiprocessing.Pool(processes=jobs) as pool:
        results = []
        for input_path in input_paths:
            output_path = os.path.join(output_dir, os.path.basename(input_path)[:-3] + 'png')
            args = (input_path, output_path)
            results.append(pool.apply_async(plot_particles, args))

        for result in results:
            result.get()

    fmt = os.path.join(output_dir, os.path.basename(input_glob)[:-3] + 'png')
    fmt = fmt.replace('*', '%*')
    make_movie(fmt, os.path.join(output_dir, 'animation.mp4'))


def main(argv):
    import argparse
    parser = argparse.ArgumentParser()
    add = parser.add_argument
    add('--input-glob', type=str, required=True)
    add('--output-dir', type=str, required=True)
    add('-j', '--jobs', type=int, default=12, help="number of parallel jobs")
    args = parser.parse_args(argv)

    plot_particles_animation(args.input_glob, args.output_dir, args.jobs)


if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
