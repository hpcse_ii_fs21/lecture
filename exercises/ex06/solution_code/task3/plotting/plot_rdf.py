#!/usr/bin/env python3

from base import Plot

import numpy as np

def plot_rdf(input_path, output_path):
    mat = np.loadtxt(input_path, skiprows=1, delimiter=',')
    r = mat[:, 0]
    # count = mat[:, 1]
    rdf = mat[:, -1]

    with Plot(output_path) as (fig, ax):
        ax.axhline(1.0, color='k')
        ax.plot(r, rdf)
        ax.set_xlim(left=0.0, right=np.ceil(r.max()))
        ax.set_xlabel("$r$")
        ax.set_ylabel("$RDF(r)$")
        ax.set_title("radial distribution function")


def main(argv):
    import argparse
    parser = argparse.ArgumentParser()
    add = parser.add_argument
    add('--input', type=str, required=True)
    add('--output', type=str, required=True)
    args = parser.parse_args(argv)

    plot_rdf(args.input, args.output)


if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
