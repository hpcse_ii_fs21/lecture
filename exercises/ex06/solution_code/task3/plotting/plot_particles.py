#!/usr/bin/env python3

from base import Plot

import numpy as np

def plot_particles(input_path, output_path):
    mat = np.loadtxt(input_path, skiprows=1, delimiter=',')
    x = mat[:, 0]
    y = mat[:, 1]
    vx = mat[:, 2]
    vy = mat[:, 3]

    # avg_v = (vx * vx + vy * vy).mean() ** 0.5
    # print(f"<|v|> = {avg_v}", (vx * vx + vy * vy).sum())

    with Plot(output_path) as (fig, ax):
        ax.scatter(x, y, s=0.5)
        ax.set_xlim(left=0.0, right=np.ceil(x.max()))
        ax.set_ylim(bottom=0.0, top=np.ceil(y.max()))
        ax.set_aspect(1.0)


def main(argv):
    import argparse
    parser = argparse.ArgumentParser()
    add = parser.add_argument
    add('--input', type=str, required=True)
    add('--output', type=str, required=True)
    args = parser.parse_args(argv)

    plot_particles(args.input, args.output)


if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
