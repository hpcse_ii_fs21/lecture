import json
import numpy as np

from base import Plot

def plot_credible_intervals(path, x, data, plot_path):
    with open(path) as f:
        d = json.load(f)

    var = 'Evaluations'
    percentages = [0.8, 0.9, 0.99]

    # Draw num_samples samples from the generative model.
    # If the plots are not smooth increase the number.
    num_samples = 100

    N  = len(d['Samples'])
    Ny = len(d['Samples'][0][var])

    y = np.zeros((N, Ny))  # Value.
    s = np.zeros((N, Ny))  # Sigma.
    for k in range(N):
        y[k, :] = d['Samples'][k][var]
        s[k, :] = d['Samples'][k]['Parameters'][-1]

    # Draw the samples from the generative model (likelihood).
    samples = np.random.normal(loc=y, scale=s)

    # Compute and plot statistics.
    median = np.quantile(samples, 0.5, axis=0)
    mean = np.mean(samples, axis=0)

    with Plot(plot_path) as (fig, ax):
        for p in sorted(percentages)[::-1]:
            q1 = np.quantile(samples, 0.5 - p/2, axis=0)
            q2 = np.quantile(samples, 0.5 + p/2, axis=0)
            ax.fill_between(x, q1, q2, alpha=0.5, label=f"{100*p:.1f}% credible interval")

        ax.plot(x, mean, '-', lw=2, label="Mean", color='black')
        ax.plot(x, median, '--', lw=2, label="Median", color='black')
        ax.plot(x, data, '.', color='red', markersize=8)

        ax.legend(loc='lower right')
        ax.grid()
        ax.set_xlim(left=x[1])
