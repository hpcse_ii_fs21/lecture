#pragma once

#include "scan.h"

struct Interaction;

/// Cell list metadata used from various kernels.
struct CellListInfo {
    /// Inverse cell size.
    double2 invCellSize;

    /// Number of cells for each dimension.
    int2 numCells;

    /// Pointer to the offsets array.
    int *offsets;

    // Add here any utility functions here you might find useful
    // (e.g. computing cell index from coordinates).

    __device__ int2 getCell(double2 p) const {
        const int cix = (int)(p.x * invCellSize.x);
        const int ciy = (int)(p.y * invCellSize.y);
        return {cix, ciy};
    }

    __device__ int getCellIndex(double2 p) const {
        const int2 c = getCell(p);
        const int cIdx = c.y * numCells.x + c.x;
        return cIdx;
    }
};

class CellList {
public:
    /// Create cell list that spans across the given domain with given cell size.
    CellList(double2 domainSize, double cellSize);
    ~CellList();

    /// Build cell list for given particles and reorder the particle positions and velocities.
    void build(const double2 *pDev, const double2 *vDev,
               double2 *pSortedDev, double2 *vSortedDev, int numParticles);

    /// Return the cell list info / metadata.
    CellListInfo getInfo() const;
private:
    Scan scan_;

    double2 invCellSize_;
    int2 numCells_;
    int *countsDev_;
    int *offsetsDev_;
};
