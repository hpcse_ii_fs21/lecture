#include "rdf.h"
#include "../../include/utils.h"
#include <cmath>
#include <cstdio>
#include <cstring>

RDF::RDF(double2 domainSize, double margin, double binSize, int numBins) :
    low_{margin, margin},
    high_{domainSize.x - margin, domainSize.y - margin},
    binSize_{binSize},
    invBinSize_{1 / binSize},
    numBins_{numBins}
{
    // Allocate the bins. To avoid having a separate buffer only for the total
    // number of samples (total number of reference points), we use b[K] as the
    // total sample counter.

    // One extra bin used for the total number of samples.
    CUDA_CHECK(cudaMalloc(&binsDev_, (numBins + 1) * sizeof(binsDev_[0])));
    CUDA_CHECK(cudaMallocHost(&binsHost_, (numBins + 1) * sizeof(binsHost_[0])));

    // Reset all initial values to 0, important!
    CUDA_CHECK(cudaMemset(binsDev_, 0, (numBins + 1) * sizeof(binsDev_[0])));
}

RDF::~RDF() {
    CUDA_CHECK(cudaFreeHost(binsHost_));
    CUDA_CHECK(cudaFree(binsDev_));
}


__global__ void updateRDFKernelWithoutShared(
        double2 low,
        double2 high,
        CellListInfo info,
        int2 cellSpan,
        const double2 *p,
        double invBinSize,
        int numBins,
        int numParticles,
        int *bins) {
    // One thread == one particles.
    const int pid = blockIdx.x * blockDim.x + threadIdx.x;
    // The particle is considered a reference point (sample) if it is under the
    // low--high bounds (i.e. not too close to the domain boundary).
    const bool active =
            pid < numParticles &&
            low.x <= p[pid].x && p[pid].x <= high.x &&
            low.y <= p[pid].y && p[pid].y <= high.y;
    if (active) {
        const int2 c = info.getCell(p[pid]);
        const int cMinX = max(c.x - cellSpan.x, 0);
        const int cMinY = max(c.y - cellSpan.y, 0);
        const int cMaxX = min(c.x + cellSpan.x, info.numCells.x - 1);
        const int cMaxY = min(c.y + cellSpan.y, info.numCells.y - 1);
        const double2 myPos = p[pid];
        for (int cY = cMinY; cY <= cMaxY; ++cY) {
            const int cIdxMin = cY * info.numCells.x + cMinX;
            const int cIdxMax = cY * info.numCells.x + cMaxX;
            const int pIdxBegin = info.offsets[cIdxMin];
            const int pIdxEnd   = info.offsets[cIdxMax + 1];
#pragma unroll 4
            for (int pIdx = pIdxBegin; pIdx < pIdxEnd; ++pIdx) {
                if (pIdx == pid)
                    continue;
                const double dx = p[pIdx].x - myPos.x;
                const double dy = p[pIdx].y - myPos.y;
                const double r = std::sqrt(dx * dx + dy * dy);
                const int bin = (int)(r * invBinSize);
                if (bin < numBins)
                    atomicAdd(&bins[bin], 1);
            }
        }
    }

    // Note: the __ballot_sync + __popc optimization was not required.

    // Compute a bitmask of lanes with an active sample.
    const unsigned activeLanes = __ballot_sync(0xFFFFFFFF, active);

    // The lane 0 of each warp counts 1s of the bitmask and updates the total
    // sample count (the extra bin).
    if ((threadIdx.x & (warpSize - 1)) == 0)
        atomicAdd(&bins[numBins], __popc(activeLanes));
}




__global__ void updateRDFKernelWithShared(
        double2 low,
        double2 high,
        CellListInfo info,
        int2 cellSpan,
        const double2 *p,
        double invBinSize,
        int numBins,
        int numParticles,
        int *bins) {
    extern __shared__ int localBins[];  // numBins + 1

    // Reset local counters to 0.
    for (int i = threadIdx.x; i < numBins + 1; i += blockDim.x)
        localBins[i] = 0;

    __syncthreads();

    // One thread == one particles.
    const int pid = blockIdx.x * blockDim.x + threadIdx.x;
    // The particle is considered a reference point (sample) if it is under the
    // low--high bounds (i.e. not too close to the domain boundary).
    const bool active =
            pid < numParticles &&
            low.x <= p[pid].x && p[pid].x <= high.x &&
            low.y <= p[pid].y && p[pid].y <= high.y;
    if (active) {
        const int2 c = info.getCell(p[pid]);
        const int cMinX = max(c.x - cellSpan.x, 0);
        const int cMinY = max(c.y - cellSpan.y, 0);
        const int cMaxX = min(c.x + cellSpan.x, info.numCells.x - 1);
        const int cMaxY = min(c.y + cellSpan.y, info.numCells.y - 1);
        const double2 myPos = p[pid];
        for (int cY = cMinY; cY <= cMaxY; ++cY) {
            const int cIdxMin = cY * info.numCells.x + cMinX;
            const int cIdxMax = cY * info.numCells.x + cMaxX;
            const int pIdxBegin = info.offsets[cIdxMin];
            const int pIdxEnd   = info.offsets[cIdxMax + 1];
#pragma unroll 4
            for (int pIdx = pIdxBegin; pIdx < pIdxEnd; ++pIdx) {
                if (pIdx == pid)
                    continue;
                const double dx = p[pIdx].x - myPos.x;
                const double dy = p[pIdx].y - myPos.y;
                const double r = std::sqrt(dx * dx + dy * dy);
                const int bin = (int)(r * invBinSize);
                if (bin < numBins)
                    atomicAdd(&localBins[bin], 1);
            }
        }
    }

    /*
    // Compute a bitmask of lanes with an active sample.
    const unsigned activeLanes = __ballot_sync(0xFFFFFFFF, active);

    // The lane 0 of each warp counts 1s of the bitmask and updates the total
    // sample count (the extra bin).
    if ((threadIdx.x & (warpSize - 1)) == 0)
        atomicAdd(&localBins[numBins], __popc(activeLanes));

    __syncthreads();
    */

    // Since we are already synchronizing the whole block, we can use
    // __syncthreads_count + 1 global atomicAdd instead of __ballot_sync +
    // __popc + 256/32 local atomicAdds to count the number of samples handled
    // by this block.
    int count = __syncthreads_count(active);

    if (threadIdx.x == 0)
        atomicAdd(&bins[numBins], count);

    // Add local counters to the global ones.
    for (int i = threadIdx.x; i < numBins + 1; i += blockDim.x)
        atomicAdd(&bins[i], localBins[i]);
}

void RDF::updateRDF(
        const CellList &cellList,
        const double2 *p,
        int numParticles) {
    const CellListInfo info = cellList.getInfo();
    const int2 cellSpan{
        (int)std::ceil(numBins_ * binSize_ * info.invCellSize.x),
        (int)std::ceil(numBins_ * binSize_ * info.invCellSize.y),
    };
    // `threads` should be a multiple of 32 because of the __ballot_sync in the kernel.
    const int threads = 256;
    const int blocks = (numParticles + threads - 1) / threads;

    constexpr bool useShared = true;

    if (useShared) {
        const int sharedMemSize = (numBins_ + 1) * sizeof(int);
        updateRDFKernelWithShared<<<blocks, threads, sharedMemSize>>>(
                low_, high_, info, cellSpan, p, invBinSize_,
                numBins_, numParticles, binsDev_);
    } else {
        updateRDFKernelWithoutShared<<<blocks, threads>>>(
                low_, high_, info, cellSpan, p, invBinSize_,
                numBins_, numParticles, binsDev_);
    }

    ++numUpdates_;
}

void RDF::dumpRDF(const char *path, bool quiet) {
    CUDA_CHECK(cudaMemcpy(binsHost_, binsDev_, (numBins_ + 1) * sizeof(binsDev_[0]),
                          cudaMemcpyDeviceToHost));

    const int totalSamples = binsHost_[numBins_];
    const double area = (high_.x - low_.x) * (high_.y - low_.y);
    const double numberDensity = totalSamples / (numUpdates_ * area);

    if (!quiet) {
        printf("Saving RDF to \"%s\". Collected %d samples in %d updates.\n",
               path, totalSamples, numUpdates_);
        // This number density will not match exactly the density in main.cu
        // because of the boundary effects of the non-periodic bounce-back
        // domain.
        // printf("Estimated number density: %g / unit square\n", numberDensity);
    }

    const bool isStdout = strcmp(path, "stdout") == 0;
    FILE *f;
    if (isStdout) {
        f = stdout;
    } else if ((f = fopen(path, "w")) == nullptr) {
        fprintf(stderr, "Error opening file \"%s\" in write mode, maybe the folder is missing?\n",
                path);
        exit(1);
    }

    fprintf(f, "r,count,rdf\n");
    for (int i = 0; i < numBins_; ++i) {
        const double r0 = i * binSize_;
        const double r1 = (i + 1) * binSize_;
        const double binArea = M_PI * (r1 * r1 - r0 * r0);
        const double rdf = binsHost_[i] / (binArea * numberDensity * totalSamples);
        fprintf(f, "%g,%d,%g\n", r0, binsHost_[i], rdf);
    }

    if (!isStdout)
        fclose(f);
}
