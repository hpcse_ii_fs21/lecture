#include "cell_list.h"
#include "interaction.h"
#include "../../include/utils.h"
#include <algorithm>

__global__ void countParticlePerCell(
        CellListInfo info, int *counts, const double2 *p, int numParticles) {
    for (int i = blockIdx.x * blockDim.x + threadIdx.x;
             i < numParticles;
             i += gridDim.x * blockDim.x) {
        const int cIdx = info.getCellIndex(p[i]);
        atomicAdd(&counts[cIdx], 1);
    }
}

__global__ void rearrangeParticles(
        CellListInfo info,
        int *counts,
        const double2 *p,
        const double2 *v,
        double2 *pNew,
        double2 *vNew,
        int numParticles) {
    for (int i = blockIdx.x * blockDim.x + threadIdx.x;
             i < numParticles;
             i += gridDim.x * blockDim.x) {
        const int cIdx = info.getCellIndex(p[i]);
        const int newIndex = info.offsets[cIdx] + atomicAdd(&counts[cIdx], 1);
        pNew[newIndex] = p[i];
        vNew[newIndex] = v[i];
    }
}

CellList::CellList(double2 domainSize, double cellSize) {
    numCells_.x = std::max(1, (int)std::floor(domainSize.x / cellSize));
    numCells_.y = std::max(1, (int)std::floor(domainSize.y / cellSize));
    invCellSize_.x = numCells_.x / domainSize.x;
    invCellSize_.y = numCells_.y / domainSize.y;

    const int totalCells = numCells_.x * numCells_.y;
    CUDA_CHECK(cudaMalloc(&countsDev_, totalCells * sizeof(int)));
    CUDA_CHECK(cudaMalloc(&offsetsDev_, (totalCells + 1) * sizeof(int)));
    // Set offsets[0] to 0.
    CUDA_CHECK(cudaMemset(offsetsDev_, 0, 1 * sizeof(int)));
}

CellList::~CellList() {
    CUDA_CHECK(cudaFree(offsetsDev_));
    CUDA_CHECK(cudaFree(countsDev_));
}

/*
__global__ void printInfo(CellListInfo info, const int *counts) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < info.numCells.x * info.numCells.y) {
        printf("idx=%d counts=%d offsets=%d\n",
               idx, counts[idx], info.offsets[idx]);
    }
}
*/

void CellList::build(const double2 *pDev, const double2 *vDev,
                     double2 *pSortedDev, double2 *vSortedDev, int numParticles) {
    const int threads = 256;
    const int blocks = (numParticles + threads - 1) / threads;
    const CellListInfo info = getInfo();

    // Stage 1: compute cell sizes and compute ofsets.
    CUDA_CHECK(cudaMemset(countsDev_, 0, numCells_.x * numCells_.y * sizeof(int)));
    countParticlePerCell<<<blocks, threads>>>(info, countsDev_, pDev, numParticles);
    scan_.inclusiveSum(countsDev_, offsetsDev_ + 1, numCells_.x * numCells_.y);

    // Stage 2: rearrange particles into cells.
    CUDA_CHECK(cudaMemset(countsDev_, 0, numCells_.x * numCells_.y * sizeof(int)));
    rearrangeParticles<<<blocks, threads>>>(
            info, countsDev_, pDev, vDev, pSortedDev, vSortedDev, numParticles);
}

CellListInfo CellList::getInfo() const {
    return {invCellSize_, numCells_, offsetsDev_};
}
