#pragma once

#include "cell_list.h"
#include "interaction.h"

class Solver {
public:
    Solver(Interaction interaction, double2 domainSize,
           double vStdDev, double berendsenTau, int numParticles,
           unsigned long long seed);
    ~Solver();

    /** Perform either a velocity-Verlet integration or a minimization step.

        Minimization is performed if maxMinimizationMove is provided and larger than 0.
    */
    void timestep(double dt, double maxMinimizationMove = 0);

    const double2 *getParticlePositions() const { return pDev_; }
    const CellList &getCellList() const { return cellList_; }

    /// Save particle positions and velocities to a CSV file.
    void dumpParticlesCSV(const char *path);

private:
    void _berendsenThermostat(double dt);

    const Interaction interaction_;
    const double2 domainSize_;
    const double vStdDev_;
    const int numParticles_;
    const double berendsenTau_;

    double2 *pOldDev_;
    double2 *vOldDev_;
    double2 *pDev_;
    double2 *vDev_;
    double2 *fDev_;
    double2 *pHost_;
    double2 *vHost_;
    double *vReductionBuffer_;
    int numReductionBlocks_;

    CellList cellList_;
};
