Task 1: Inclusive Scan
----------------------

To compile the code, first install the ``meson`` and ``ninja`` dependencies:

.. code-block::

    python3 -m pip install --user meson
    python3 -m pip install --user ninja


Then, run the following from the ``task1/`` folder:

.. code-block::

    meson setup build
    cd build
    ninja


NOTE: meson currently does not fully support nvcc and it does not track header dependencies. If you modify header files, run ``ninja clean`` and recompile with ``ninja``.

The executable ``inscan`` in the ``build`` folder can be used as follows:

.. code-block::

   # Show all flags.
   ./inscan --help

   # Run all tests and benchmarks.
   ./inscan

   # N <= 32
   ./inscan --warp

   # 32 < N <= 1024
   ./inscan --block

   # 1024 < N <= 1024^2
   ./inscan --medium

   # N > 1024^2
   ./inscan --large

   # For profiling:
   nvprof --print-gpu-trace ./inscan --medium --profile
   nvprof --print-gpu-trace ./inscan --large --profile
