#pragma once

#include <cstddef>

class Scan {
public:
    Scan();
    ~Scan();

    /** Compute the inclusive sum.

        For example, for an input array
            {5, 3,  5,  1,  2,  0,  2}
        compute
            {5, 8, 13, 14, 16, 16, 18}.
    */
    void inclusiveSum(const int *inDev, int *outDev, int N);

private:
    void _inclusiveSumABC(const int *inDev, int *outDev, int N);
    void _inclusiveSumD(const int *inDev, int *outDev, int N);

    int *tmpDev_ = nullptr;
    int tmpSize_ = 0;
};
