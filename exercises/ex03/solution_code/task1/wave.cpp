#include <cmath>
#include <iomanip>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "wave.hpp"

//
// Function defining the initial displacement on the grid
//
double f(double x, double y)
{
    double r = (x - 0.5) * (x - 0.5) + (y - 0.5) * (y - 0.5);
    return 0.3 * exp(-5.0 * r / 0.1);
}

WaveEquation::WaveEquation(size_t a_gridpoints_per_dim,
                           size_t a_procs_per_dim,
                           double a_t_end,
                           MPI_Comm a_comm)
    : Ntot(a_gridpoints_per_dim), procs_per_dim(a_procs_per_dim),
      t_end(a_t_end), cart_comm(a_comm)
{
    h = L / Ntot;
    N = Ntot / procs_per_dim;
    size_t N_halo = N + 2;

    dt = h / sqrt(3.0);
    c_aug = dt * dt / (h * h);

    u.resize(N_halo * N_halo);
    u_old.resize(N_halo * N_halo);
    u_new.resize(N_halo * N_halo);

    //
    // MPI related initializations
    //
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int periodic[2] = {true, true};
    // Set no restrictions to the number of processes on any dimension
    nums[0] = 0;
    nums[1] = 0;
    MPI_Dims_create(size, 2, nums);

    MPI_Cart_create(MPI_COMM_WORLD, 2, nums, periodic, true, &cart_comm);

    MPI_Comm_rank(cart_comm, &rank);
    MPI_Cart_shift(cart_comm, 0, 1, &rank_minus[0], &rank_plus[0]);
    MPI_Cart_shift(cart_comm, 1, 1, &rank_minus[1], &rank_plus[1]);
    if (rank == 0) {
        printf("(%d, %d) processes mapping to a (%d, %d) grid\n",
               nums[0],
               nums[1],
               N,
               N);
    }

    //
    // Find its location in the simulation space
    //
    MPI_Cart_coords(cart_comm, rank, 2, &coords[0]);
    origin[0] = N * h * coords[0];
    origin[1] = N * h * coords[1];

    //
    // Set initial conditions (inclusive first time derivative)
    //
    initializeGrid();

    for (size_t i = 0; i < N; ++i) {
        for (size_t j = 0; j < N; ++j) {
            size_t idx = (i + 1) * (N + 2) + (j + 1);
            u_old[idx] = u[idx];
            u_new[idx] = u[idx];
        }
    }
}

void WaveEquation::run()
{
    double t = 0.0;
    unsigned int count = 0;

    //
    // Create datatypes to communicate halo boundaries
    //
    // // Possible solution with following types
    // MPI_Datatype horizontal_halo_t, vertical_halo_t;

    // MPI_Type_contiguous(N, MPI_DOUBLE, &horizontal_halo_t);
    // MPI_Type_commit(&horizontal_halo_t);

    // int blocklength = N;
    // int stride = N+2;
    // MPI_Type_vector(N, blocklength, stride, MPI_DOUBLE, &vertical_halo_t);
    // MPI_Type_commit(&vertical_halo_t);

    MPI_Datatype SEND_HALO_PLUS[2];
    MPI_Datatype SEND_HALO_MINUS[2];

    MPI_Datatype RECV_HALO_PLUS[2];
    MPI_Datatype RECV_HALO_MINUS[2];

    int ndims = 2;
    int order = MPI_ORDER_C;
    int sizes[2] = {N + 2, N + 2};
    int subsizes[2];
    int starts[2];

    for (int d0 = 0; d0 < 2; ++d0) {
        int d1 = (d0 + 1) % 2;
        subsizes[d0] = 1;
        subsizes[d1] = N;

        starts[d0] = 1;
        starts[d1] = 1;
        MPI_Type_create_subarray(ndims,
                                 sizes,
                                 subsizes,
                                 starts,
                                 order,
                                 MPI_DOUBLE,
                                 &SEND_HALO_MINUS[d0]);

        starts[d0] = N;
        starts[d1] = 1;
        MPI_Type_create_subarray(ndims,
                                 sizes,
                                 subsizes,
                                 starts,
                                 order,
                                 MPI_DOUBLE,
                                 &SEND_HALO_PLUS[d0]);

        starts[d0] = 0;
        starts[d1] = 1;
        MPI_Type_create_subarray(ndims,
                                 sizes,
                                 subsizes,
                                 starts,
                                 order,
                                 MPI_DOUBLE,
                                 &RECV_HALO_MINUS[d0]);

        starts[d0] = N + 1;
        starts[d1] = 1;
        MPI_Type_create_subarray(ndims,
                                 sizes,
                                 subsizes,
                                 starts,
                                 order,
                                 MPI_DOUBLE,
                                 &RECV_HALO_PLUS[d0]);

        MPI_Type_commit(&SEND_HALO_PLUS[d0]);
        MPI_Type_commit(&RECV_HALO_PLUS[d0]);
        MPI_Type_commit(&SEND_HALO_MINUS[d0]);
        MPI_Type_commit(&RECV_HALO_MINUS[d0]);
    }
    /*  // Unrolled version
        subsizes[0] = 1;
        subsizes[1] = N;

        starts[0] = 1;
        starts[1] = 1;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order,
       MPI_DOUBLE, &SEND_HALO_MINUS[0]);

        starts[0] = N;
        starts[1] = 1;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order,
       MPI_DOUBLE, &SEND_HALO_PLUS[0]);

        starts[0] = 0;
        starts[1] = 1;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order,
       MPI_DOUBLE, &RECV_HALO_MINUS[0]);

        starts[0] = N+1;
        starts[1] = 1;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order,
       MPI_DOUBLE, &RECV_HALO_PLUS[0]);

        //=======================

        subsizes[0] = N;
        subsizes[1] = 1;

        starts[0] = 1;
        starts[1] = 1;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order,
       MPI_DOUBLE, &SEND_HALO_MINUS[1]);

        starts[d0] = 1;
        starts[d1] = N;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order,
       MPI_DOUBLE, &SEND_HALO_PLUS[1]);

        starts[d0] = 1;
        starts[d1] = 0;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order,
       MPI_DOUBLE, &RECV_HALO_MINUS[1]);

        starts[d0] = 1;
        starts[d1] = N+1;
        MPI_Type_create_subarray(ndims, sizes, subsizes, starts, order,
       MPI_DOUBLE, &RECV_HALO_MINUS[1]);
    */

    //
    // Main loop propagating the solution forward in time
    //
    while (t < t_end) {
        if (count % 10 == 0) {
            saveGrid(count);
            double l2_norm = computeSquaredL2Norm();
            if (rank == 0)
                printf("t=%i : L2-Norm = %f\n", count, l2_norm);
        }

        // Send and receive halo boundaries
        MPI_Request request[8];
        for (int d = 0; d < 2; ++d) {
            MPI_Irecv(u.data(),
                      1,
                      RECV_HALO_MINUS[d],
                      rank_minus[d],
                      d,
                      cart_comm,
                      &request[2 * d]);
            MPI_Irecv(u.data(),
                      1,
                      RECV_HALO_PLUS[d],
                      rank_plus[d],
                      d + 1,
                      cart_comm,
                      &request[2 * d + 1]);
        }

        for (int d = 0; d < 2; ++d) {
            MPI_Isend(u.data(),
                      1,
                      SEND_HALO_PLUS[d],
                      rank_plus[d],
                      d,
                      cart_comm,
                      &request[4 + 2 * d]);
            MPI_Isend(u.data(),
                      1,
                      SEND_HALO_MINUS[d],
                      rank_minus[d],
                      d + 1,
                      cart_comm,
                      &request[4 + 2 * d + 1]);
        }

        MPI_Waitall(8, &request[0], MPI_STATUSES_IGNORE);

        // For Debugging purposes (with np=1)!!!!! (periodic b.c.)
        // for(size_t i = 1; i < N+1; ++i){
        //    // bottom
        //    u[i] = u[(N+2)*(N)+i];
        //    // top
        //    u[(N+2)*(N+1)+i] = u[(N+2)+i];
        //    //left
        //    u[i*(N+2)] = u[i*(N+2) + (N+1)];
        //    //right
        //    u[i*(N+2)+(N+1)] = u[i*(N+2) + 1];
        //}

        //
        // Update the cells with FD stencil
        //
        for (size_t i = 1; i < N + 1; ++i) {
            for (size_t j = 1; j < N + 1; ++j) {
                applyStencil(i, j);
            }
        }

        //
        // Swap vectors
        //
        u_old.swap(u);
        u.swap(u_new);

        //
        // Update time
        //
        t += dt;
        count++;
    }

    //
    // Free communication datatypes
    //
    for (int d = 0; d < 2; d++) {
        MPI_Type_free(&RECV_HALO_PLUS[d]);
        MPI_Type_free(&RECV_HALO_MINUS[d]);
        MPI_Type_free(&SEND_HALO_PLUS[d]);
        MPI_Type_free(&SEND_HALO_MINUS[d]);
    }
}

void WaveEquation::initializeGrid()
{
    double x_pos, y_pos;
    for (size_t i = 0; i < N; ++i) {
        x_pos = origin[0] + i * h + 0.5 * h;
        for (size_t j = 0; j < N; ++j) {
            y_pos = origin[1] + j * h + 0.5 * h;
            u[(i + 1) * (N + 2) + (j + 1)] = f(x_pos, y_pos);
        }
    }
}

double WaveEquation::computeSquaredL2Norm() const
{
    double l2_norm = 0.0;
    for (size_t i = 0; i < N; ++i) {
        for (size_t j = 0; j < N; ++j) {
            l2_norm +=
                u[(i + 1) * (N + 2) + (j + 1)] * u[(i + 1) * (N + 2) + (j + 1)];
        }
    }
    MPI_Reduce((rank == 0) ? MPI_IN_PLACE : &l2_norm,
               &l2_norm,
               1,
               MPI_DOUBLE,
               MPI_SUM,
               0,
               cart_comm);
    return h * l2_norm;
}

void WaveEquation::applyStencil(size_t i, size_t j)
{
    size_t center = i * (N + 2) + j;
    u_new[center] =
        2.0 * u[center] - u_old[center] +
        c_aug * (u[(i + 1) * (N + 2) + j] + u[(i - 1) * (N + 2) + j] +
                 u[i * (N + 2) + (j + 1)] + u[i * (N + 2) + (j - 1)] -
                 4.0 * u[center]);
}

void WaveEquation::saveGrid(size_t timestep) const
{
    std::stringstream ss;
    ss << "./output/wave_" << std::setfill('0') << std::setw(3) << timestep
       << ".bin";
    std::string fname = ss.str();

    // Create derived datatype for interior grid (output grid)
    MPI_Datatype grid;
    const int start[2] = {1, 1};
    const int arrsize[2] = {N + 2, N + 2};
    const int gridsize[2] = {N, N};

    MPI_Type_create_subarray(
        2, arrsize, gridsize, start, MPI_ORDER_C, MPI_DOUBLE, &grid);
    MPI_Type_commit(&grid);

    // Create derived type for file view
    MPI_Datatype view;
    const int startV[2] = {coords[0] * N, coords[1] * N};
    const int arrsizeV[2] = {nums[0] * N, nums[1] * N};
    const int gridsizeV[2] = {N, N};

    MPI_Type_create_subarray(
        2, arrsizeV, gridsizeV, startV, MPI_ORDER_C, MPI_DOUBLE, &view);
    MPI_Type_commit(&view);

    MPI_File fh;

    MPI_File_open(cart_comm,
                  fname.c_str(),
                  MPI_MODE_CREATE | MPI_MODE_WRONLY,
                  MPI_INFO_NULL,
                  &fh);

    MPI_File_set_view(fh, 0, MPI_DOUBLE, view, "native", MPI_INFO_NULL);
    MPI_File_write_all(fh, u.data(), 1, grid, MPI_STATUS_IGNORE);
    MPI_File_close(&fh);
}

WaveEquation::~WaveEquation() { MPI_Comm_free(&cart_comm); }
