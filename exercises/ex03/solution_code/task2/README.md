### Exercise 3 -- Task 2

Use the Makefile to compile the program.
Run with `mpirun -n 2 n_body`

Load following modules on euler:

```
. /cluster/apps/local/env2lmod.sh
module load gcc/8.2.0  cmake/3.9.4 openmpi/4.0.2
export MPICXX=`which mpicxx`
```
