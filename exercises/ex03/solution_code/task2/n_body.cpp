#include <cassert>
#include <iostream>
#include <mpi.h>
#include <unistd.h>
#include <vector>

//
// Data Structures
//

struct particle {
    float x;
    float y;
    float z;
    float mass;
};

struct system_positions {
    std::vector<float> x;
    std::vector<float> y;
    std::vector<float> z;
};

//
// Helper Functions (unimportant)
//

void fill_particles(std::vector<particle> &system)
{
    for (unsigned int i = 0; i < system.size(); ++i) {
        system[i].x = i * 0.1;
        system[i].y = i * 0.5;
        system[i].z = i * 0.8;
        system[i].mass = i * i * 0.4;
    }
}

void print(std::vector<particle> const &system)
{
    for (unsigned int i = 0; i < system.size(); ++i)
        std::cout << system[i].x << ", " << system[i].y << ", " << system[i].z
                  << "\t mass: " << system[i].mass << "\n";
}

void print(system_positions const &system)
{
    assert(system.x.size() == system.y.size());
    assert(system.x.size() == system.z.size());
    for (unsigned int i = 0; i < system.x.size(); ++i)
        std::cout << system.x[i] << ", " << system.y[i] << ", " << system.z[i]
                  << "\n";
}

//
// The MPI Program
//

int main(int argc, char **argv)
{
    ////////////////   Init MPI    ////////////////

    unsigned int n = 10;
    MPI_Init(&argc, &argv);
    int comm_size;
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    if (comm_size != 2) {
        std::cerr << "ERROR: Needs to run with 2 MPI processes." << std::endl;
        MPI_Finalize();
        return -1;
    }
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    ////////////////  Question a)  ////////////////
    if (rank == 0)
        std::cout << std::endl
                  << std::endl
                  << "Beginning of Question a)" << std::endl;

    // Use an array(vector) of structures (AoS) to store the data
    std::vector<particle> system(n);
    if (rank == 0) {
        fill_particles(system);

        // Print data to be sent
        std::cout << "Rank 0\n";
        print(system);
        std::cout << std::endl;
    }

    //
    // TODO:
    // * Create an MPI datatype for all the position parts of 'system'
    // * Send all positions from rank 0 to rank 1
    // Do not send any masses

    MPI_Aint p_lb, p_pos, p_ub, p_extent;
    int blocklens[] = {3};
    MPI_Datatype types[] = {MPI_FLOAT};
    MPI_Get_address(&system[0], &p_lb);
    MPI_Get_address(&system[0].x, &p_pos);
    MPI_Aint offsets[] = {MPI_Aint_diff(p_pos, p_lb)};

    MPI_Datatype tmp_t, aos_particle_t;
    MPI_Type_create_struct(1, blocklens, offsets, types, &tmp_t);

    // Compute the real extent of the full struct to adapt the
    // ub (upper bound) and lb (lower bound) markers of the struct
    MPI_Get_address(&system[1], &p_ub);
    p_extent = MPI_Aint_diff(p_ub, p_lb);

    MPI_Type_create_resized(tmp_t, p_lb, p_extent, &aos_particle_t);
    MPI_Type_commit(&aos_particle_t);

    if (rank == 0) {
        MPI_Send(system.data(), n, aos_particle_t, 1, 42, MPI_COMM_WORLD);
    } else {
        MPI_Recv(system.data(),
                 n,
                 aos_particle_t,
                 0,
                 42,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);
    }

    if (rank == 1) {
        // Print received data
        std::cout << "Rank 1\n";
        print(system);
        std::cout << std::endl;
    }

    ////////////////  Question b)  ////////////////
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0)
        std::cout << std::endl
                  << std::endl
                  << "Beginning of Question b)" << std::endl;

    // Use a structure of arrays(vectors) (SoA) to store the positions
    system_positions positions;
    positions.x.resize(n);
    positions.y.resize(n);
    positions.z.resize(n);

    //
    // Note the different data layout in this task!
    //
    // TODO:
    // * Create an MPI datatype for the positions in SoA format
    // * Send system from rank 0 to rank 1 (same send as the previous task)
    // * Receive the data in the `positions` variable on rank 1
    //

    MPI_Aint pos_lb, pos_x0, pos_y0, pos_z0;
    int blocklens_triple[] = {1, 1, 1};
    MPI_Datatype types_triple[] = {MPI_FLOAT, MPI_FLOAT, MPI_FLOAT};
    MPI_Get_address(&positions, &pos_lb);
    MPI_Get_address(&positions.x[0], &pos_x0);
    MPI_Get_address(&positions.y[0], &pos_y0);
    MPI_Get_address(&positions.z[0], &pos_z0);
    MPI_Aint offsets_triple[] = {MPI_Aint_diff(pos_x0, pos_lb),
                                MPI_Aint_diff(pos_y0, pos_lb),
                                MPI_Aint_diff(pos_z0, pos_lb)};

    MPI_Type_create_struct(
        3, blocklens_triple, offsets_triple, types_triple, &tmp_t);

    // Compute stride between consecutive elements of struct type
    MPI_Aint pos_x1;
    MPI_Get_address(&positions.x[1], &pos_x1);
    MPI_Aint stride = MPI_Aint_diff(pos_x1, pos_x0);

    // 1st variant: Define new particle_t by creating a hvector derived from tmp_t
    MPI_Datatype soa_particle_t;
    int blocklens_soa = 1;
    MPI_Type_create_hvector(n, blocklens_soa, stride, tmp_t, &soa_particle_t);
    MPI_Type_commit(&soa_particle_t);

    if (rank == 0) {
        MPI_Send(system.data(), n, aos_particle_t, 1, 42, MPI_COMM_WORLD);
    } else {
        MPI_Recv(&positions,
                 1,
                 soa_particle_t,
                 0,
                 42,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);
    }

/*
 *    // 2nd variant: Define new particle_t by adapting the lb and ub markers of
 *    // tmp_t with MPI_Type_create_resized
 *    MPI_Datatype soa_particle_t;
 *    MPI_Type_create_resized(tmp_t, 0, stride, &soa_particle_t);
 *    MPI_Type_commit(&soa_particle_t);
 *
 *    if (rank == 0) {
 *        MPI_Send(system.data(), n, aos_particle_t, 1, 42, MPI_COMM_WORLD);
 *    } else {
 *        MPI_Recv(&positions,
 *                 n,
 *                 soa_particle_t,
 *                 0,
 *                 42,
 *                 MPI_COMM_WORLD,
 *                 MPI_STATUS_IGNORE);
 *    }
 */

    // Print received data
    if (rank == 1) {
        // Print received data
        print(positions);
        std::cout << std::endl;
    }

    ////////////////   Cleanup    ////////////////
    MPI_Type_free(&aos_particle_t);
    MPI_Type_free(&soa_particle_t);

    MPI_Finalize();
    return 0;
}
