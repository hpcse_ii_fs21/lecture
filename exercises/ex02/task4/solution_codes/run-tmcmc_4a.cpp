#include "model/heat2d.hpp"
#include "korali.hpp"

int main(int argc, char* argv[]) {

  if (argc!=3) {
      printf("\nusage: %s Nsamples out_dirname \n", argv[0]);
      printf("        Nsamples: number of samples per generation (TMCMC) \n");
      printf("        out_dirname: folder to output results \n");
      exit(1); }

  size_t nCandles = 1;

  auto e = korali::Experiment();
  auto p = heat2DInit(nCandles);

  e["Problem"]["Type"] = "Bayesian/Reference";
  e["Problem"]["Likelihood Model"] = "Normal";
  e["Problem"]["Reference Data"] = p.refTemp;
  e["Problem"]["Computational Model"] = &heat2DSolver;

  // Solver
  e["Solver"]["Type"] = "Sampler/TMCMC";
  size_t nSamples = atoi(argv[1]);
  printf("Running TMCMC with %d samples\n", nSamples);
  e["Solver"]["Population Size"] = nSamples;

  // Set output folder
  e["File Output"]["Path"] = argv[2];

  // Set verbosity of output
  e["Console Output"]["Verbosity"] = "Detailed";

  // Prior pdf
  e["Distributions"][0]["Name"] = "Uniform 0";
  e["Distributions"][0]["Type"] = "Univariate/Uniform";
  e["Distributions"][0]["Minimum"] = 0.0;
  e["Distributions"][0]["Maximum"] = 1.0;

  e["Distributions"][1]["Name"] = "Uniform 1";
  e["Distributions"][1]["Type"] = "Univariate/Uniform";
  e["Distributions"][1]["Minimum"] = 0.0;
  e["Distributions"][1]["Maximum"] = 1.0;

  e["Distributions"][2]["Name"] = "Uniform 2";
  e["Distributions"][2]["Type"] = "Univariate/Uniform";
  e["Distributions"][2]["Minimum"] = 0.0;
  e["Distributions"][2]["Maximum"] = 20.0;

  // Valid region for parameters
  e["Variables"][0]["Name"] = "PosX";
  e["Variables"][0]["Prior Distribution"] = "Uniform 0";
  e["Variables"][0]["Lower Bound"] = 0.0;
  e["Variables"][0]["Upper Bound"] = 1.0;

  e["Variables"][1]["Name"] = "PosY";
  e["Variables"][1]["Prior Distribution"] = "Uniform 1";
  e["Variables"][1]["Lower Bound"] = 0.0;
  e["Variables"][1]["Upper Bound"] = 1.0;

  e["Variables"][2]["Name"] = "[Sigma]";
  e["Variables"][2]["Prior Distribution"] = "Uniform 2";
  e["Variables"][2]["Lower Bound"] = 0.0;
  e["Variables"][2]["Upper Bound"] = 20.0;

  // Run the experiment
  auto k = korali::Engine();
  k.run(e);

  return 0;
}
