#!/bin/bash

m=tmcmc
NSAMPLES=(1000 10000)
for Ns in ${NSAMPLES[@]}; do
    bsub -n 1 -W 04:00 ./run-${m}_4b_U $Ns res_4b_${m}_Ns${Ns}
done

m=cmaes
NSAMPLES=(32 64 128 256)
for Ns in ${NSAMPLES[@]}; do
    bsub -n 1 -W 04:00 ./run-${m}_4b_U $Ns res_4b_${m}_Ns${Ns}
done
