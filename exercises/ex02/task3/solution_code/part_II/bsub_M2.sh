#!/bin/bash

Ns=5000

for i in `seq 1 10`; do
    echo $i
    bsub -n 1 -W 02:00 ./run-tmcmc_M2.py --Ns $Ns --outdir korali_M2_Ns${Ns}_i${i}
done
