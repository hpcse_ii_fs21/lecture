#!/usr/bin/env python3

# In this example, we demonstrate how Korali find the variable values
# that maximize the posterior in a bayesian problem where the likelihood
# is calculated by providing reference data points and their objective values.

# Importing the computational model
import sys
sys.path.append('./_model')
from model import *

# Creating new experiment
import korali
e = korali.Experiment()

# Setting up the reference likelihood for the Bayesian Problem
e["Problem"]["Type"] = "Bayesian/Reference"
e["Problem"]["Likelihood Model"] = "Normal"
e["Problem"]["Reference Data"] = getReferenceData()
e["Problem"]["Computational Model"] = lambda sampleData: model_3p_scaled(sampleData, getReferencePoints())

# Configuring CMA-ES parameters
e["Solver"]["Type"] = "Optimizer/CMAES"
e["Solver"]["Population Size"] = 128
e["Solver"]["Termination Criteria"]["Max Generations"] = 100
e["Solver"]["Termination Criteria"]["Min Value Difference Threshold"] = 1e-14

# Configuring the problem's random distributions
e["Distributions"][0]["Name"] = "Uniform 0"
e["Distributions"][0]["Type"] = "Univariate/Uniform"
e["Distributions"][0]["Minimum"] = +0.4
e["Distributions"][0]["Maximum"] = +1.2

e["Distributions"][1]["Name"] = "Uniform 1"
e["Distributions"][1]["Type"] = "Univariate/Uniform"
e["Distributions"][1]["Minimum"] = -0.5
e["Distributions"][1]["Maximum"] = +0.5

e["Distributions"][2]["Name"] = "Uniform 2"
e["Distributions"][2]["Type"] = "Univariate/Uniform"
e["Distributions"][2]["Minimum"] = -1.0
e["Distributions"][2]["Maximum"] = +1.0

e["Distributions"][3]["Name"] = "Uniform 3"
e["Distributions"][3]["Type"] = "Univariate/Uniform"
e["Distributions"][3]["Minimum"] = 0
e["Distributions"][3]["Maximum"] = +1.0

# Configuring the problem's variables
e["Variables"][0]["Name"] = "D0"
e["Variables"][0]["Prior Distribution"] = "Uniform 0"
e["Variables"][0]["Initial Value"] = +0.6
e["Variables"][0]["Initial Standard Deviation"] = +0.25

e["Variables"][1]["Name"] = "k1"
e["Variables"][1]["Prior Distribution"] = "Uniform 1"
e["Variables"][1]["Initial Value"] = +0.0
e["Variables"][1]["Initial Standard Deviation"] = +0.25

e["Variables"][2]["Name"] = "k2"
e["Variables"][2]["Prior Distribution"] = "Uniform 2"
e["Variables"][2]["Initial Value"] = +0.0
e["Variables"][2]["Initial Standard Deviation"] = +0.25

e["Variables"][3]["Name"] = "[Sigma]"
e["Variables"][3]["Prior Distribution"] = "Uniform 3"
e["Variables"][3]["Initial Value"] = +0.5
e["Variables"][3]["Initial Standard Deviation"] = +0.25

# Configuring output settings
e["File Output"]["Path"] = '_korali_result_cmaes'
e["File Output"]["Frequency"] = 1
e["Console Output"]["Frequency"] = 5

# Starting Korali's Engine and running experiment
k = korali.Engine()
k.run(e)
