#!/bin/bash
set -e

NSAMPLES=(4 8 16 32 64 128 256)

for Ns in ${NSAMPLES[@]}; do
    python3 run-cmaes.py --Ns $Ns --outdir korali_Ns${Ns}
done
