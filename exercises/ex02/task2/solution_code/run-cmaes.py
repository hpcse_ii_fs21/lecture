#!/usr/bin/env python3

"""
HPCSE II, 2021
Exercise 2 - task 2

In this exercise we optimize the Rosenbrock function,
using CMA-ES and Korali.
"""


# Import korali's engine
import korali


# Import computational model
import sys
sys.path.append('./_model')
from model import *

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--Ns', type=int, default=32)
parser.add_argument('--outdir', type=str, default="_korali_result_cmaes")
args = parser.parse_args()


# Start korali's Engine
k = korali.Engine()


# Create new experiment
e = korali.Experiment()


# Define the problem
e["Problem"]["Type"] = "Optimization"
e["Problem"]["Objective Function"] = negative_rosenbrock


# Initialize random seed
e["Random Seed"] = 0xC0FEE


# Defining variables and their bounds
e["Variables"][0]["Name"] = "x"
e["Variables"][0]["Lower Bound"] = -2.0
e["Variables"][0]["Upper Bound"] = +2.0

e["Variables"][1]["Name"] = "y"
e["Variables"][1]["Lower Bound"] = -3.0
e["Variables"][1]["Upper Bound"] = +3.0


# Choose the solver: CMAES
e["Solver"]["Type"] = "Optimizer/CMAES"


# Configure CMA-ES parameters
e["Solver"]["Population Size"] = args.Ns
e["Solver"]["Termination Criteria"]["Min Value Difference Threshold"] = 1e-14
e["Solver"]["Termination Criteria"]["Max Generations"] = 100


# Configure results path
e["File Output"]["Enabled"] = True
e["File Output"]["Path"] = args.outdir
e["File Output"]["Frequency"] = 1


# Run korali
k.run(e)

