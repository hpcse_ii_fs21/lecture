#!/usr/bin/env python
import numpy as np

def negative_rosenbrock(p):
    x = p["Parameters"][0]
    y = p["Parameters"][1]
    a = 1.0
    b = 100.0
    res = (a-x)**2 + b*(y-x*x)**2
    p["F(x)"] = -res
