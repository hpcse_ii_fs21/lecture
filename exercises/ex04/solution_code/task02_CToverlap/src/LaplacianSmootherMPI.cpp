// File       : LaplacianSmootherMPI.cpp
// Created    : Wed Apr 14 2021 03:01:05 PM (+0200)
// Description: Laplacian smoother implementation (distributed)
// Copyright 2021 ETH Zurich. All Rights Reserved.

#include "LaplacianSmootherMPI.h"
#include <cassert>
#include <chrono>
#include <fstream>
#include <string>

using namespace std::chrono;

LaplacianSmootherMPI::LaplacianSmootherMPI(const int Nx,
                                           const int Ny,
                                           const int Nz,
                                           const int Px,
                                           const int Py,
                                           const int Pz,
                                           const MPI_Comm comm_root)
    : LaplacianSmoother(Nx, Ny, Nz), comm_world_(comm_root),
      comm_cart_(MPI_COMM_NULL), procs_{Px, Py, Pz},
      recv_req_(6, MPI_REQUEST_NULL), send_req_(6, MPI_REQUEST_NULL)
{
    int size;
    MPI_Comm_size(comm_world_, &size);
    if (size != Px * Py * Pz) {
        MPI_Abort(comm_world_, 1);
    }
    const int periodic[3] = {true, true, true};
    MPI_Cart_create(comm_world_, 3, procs_, periodic, true, &comm_cart_);
    MPI_Comm_rank(comm_cart_, &rank_cart_);
    MPI_Cart_shift(comm_cart_, 0, 1, &nbr_[X0], &nbr_[X1]);
    MPI_Cart_shift(comm_cart_, 1, 1, &nbr_[Y0], &nbr_[Y1]);
    MPI_Cart_shift(comm_cart_, 2, 1, &nbr_[Z0], &nbr_[Z1]);

    // clang-format off
    // create communication datatype templates
    MPI_Type_vector(Ny, 1, N_[0], MPIDataType, &StripeX);
    MPI_Type_create_hvector(Nz, 1, N_[0] * N_[1] * sizeof(DataType), StripeX, &FaceX);
    MPI_Type_vector(Nz, Nx, N_[0] * N_[1], MPIDataType, &FaceY);
    MPI_Type_vector(Ny, Nx, N_[0], MPIDataType, &FaceZ);
    MPI_Type_commit(&StripeX);
    MPI_Type_commit(&FaceX);
    MPI_Type_commit(&FaceY);
    MPI_Type_commit(&FaceZ);
    // clang-format on
}

LaplacianSmootherMPI::~LaplacianSmootherMPI()
{
    MPI_Comm_free(&comm_cart_);
    MPI_Type_free(&StripeX);
    MPI_Type_free(&FaceX);
    MPI_Type_free(&FaceY);
    MPI_Type_free(&FaceZ);
}

void LaplacianSmootherMPI::report() const
{
    int size;
    MPI_Comm_size(comm_cart_, &size);
    std::vector<double> t_sweep(size, 0.0);
    std::vector<double> t_async(size, 0.0);
    std::vector<double> t_inner(size, 0.0);
    std::vector<double> t_ghosts(size, 0.0);
    std::vector<double> t_boundary(size, 0.0);
    // clang-format off
    MPI_Gather(&t_sweep_, 1, MPI_DOUBLE, t_sweep.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    MPI_Gather(&t_async_, 1, MPI_DOUBLE, t_async.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    MPI_Gather(&t_inner_, 1, MPI_DOUBLE, t_inner.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    MPI_Gather(&t_ghosts_, 1, MPI_DOUBLE, t_ghosts.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    MPI_Gather(&t_boundary_, 1, MPI_DOUBLE, t_boundary.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    // clang-format on

    if (0 == rank_cart_) {
        // min-max-avg
        // sweep, async, inner, ghosts, boundary
        const std::string header = "sweep\tasync\tinner\tghosts\tboundary\n";
        double mma[5][3] = {{1.0e12, 0, 0},
                            {1.0e12, 0, 0},
                            {1.0e12, 0, 0},
                            {1.0e12, 0, 0},
                            {1.0e12, 0, 0}};
        double *measure[5] = {t_sweep.data(),
                              t_async.data(),
                              t_inner.data(),
                              t_ghosts.data(),
                              t_boundary.data()};

        std::ofstream per_rank("rank_stats_" + std::to_string(procs_[0]) +
                               std::to_string(procs_[1]) +
                               std::to_string(procs_[2]) + ".dat");
        per_rank << "# rank\t" << header;
        for (int i = 0; i < size; ++i) {
            per_rank << i;
            for (int j = 0; j < 5; ++j) {
                measure[j][i] /= sweep_count_;
                per_rank << '\t' << measure[j][i];
                // clang-format off
                mma[j][0] = (measure[j][i] < mma[j][0]) ? measure[j][i] : mma[j][0];
                mma[j][1] = (measure[j][i] > mma[j][1]) ? measure[j][i] : mma[j][1];
                mma[j][2] += measure[j][i];
                // clang-format on
            }
            per_rank << '\n';
        }
        for (int j = 0; j < 5; ++j) {
            mma[j][2] /= size;
        }

        const int Nx = N_[0] - 2;
        const int Ny = N_[1] - 2;
        const int Nz = N_[2] - 2;
        // clang-format off
        std::printf("Measurement report for dimension %d x %d x %d per rank. Topology %d x %d x %d:\n",
                    Nx, Ny, Nz, procs_[0], procs_[1], procs_[2] );
        std::printf("\tSweep count:           %zu\n", sweep_count_);
        std::printf("\tAvg. time sweep:       min:%.3e, max:%.3e, avg:%.3e microseconds\n", mma[0][0], mma[0][1], mma[0][2]);
        std::printf("\tAvg. time async comm:  min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%%]\n", mma[1][0], mma[1][1], mma[1][2], 100*mma[1][2]/mma[0][2]);
        std::printf("\tAvg. time inner:       min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%%]\n", mma[2][0], mma[2][1], mma[2][2], 100*mma[2][2]/mma[0][2]);
        std::printf("\tAvg. time sync ghosts: min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%%]\n", mma[3][0], mma[3][1], mma[3][2], 100*mma[3][2]/mma[0][2]);
        std::printf("\tAvg. time boundary:    min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%%]\n", mma[4][0], mma[4][1], mma[4][2], 100*mma[4][2]/mma[0][2]);
        // clang-format on
    }
}

void LaplacianSmootherMPI::comm_()
{
    const int Nx = N_[0] - 2;
    const int Ny = N_[1] - 2;
    const int Nz = N_[2] - 2;

    // clang-format off
    // receive of 6 faces directly into ghost buffers
    MPI_Irecv(&operator()(-1, 0, 0), 1, FaceX, nbr_[X0], 100, comm_cart_, &recv_req_[0]);
    MPI_Irecv(&operator()(Nx, 0, 0), 1, FaceX, nbr_[X1], 101, comm_cart_, &recv_req_[1]);
    MPI_Irecv(&operator()(0, -1, 0), 1, FaceY, nbr_[Y0], 102, comm_cart_, &recv_req_[2]);
    MPI_Irecv(&operator()(0, Ny, 0), 1, FaceY, nbr_[Y1], 103, comm_cart_, &recv_req_[3]);
    MPI_Irecv(&operator()(0, 0, -1), 1, FaceZ, nbr_[Z0], 104, comm_cart_, &recv_req_[4]);
    MPI_Irecv(&operator()(0, 0, Nz), 1, FaceZ, nbr_[Z1], 105, comm_cart_, &recv_req_[5]);

    // send of 6 faces from internal domain
    MPI_Isend(&operator()(0, 0, 0),    1, FaceX, nbr_[X0], 101, comm_cart_, &send_req_[0]);
    MPI_Isend(&operator()(Nx-1, 0, 0), 1, FaceX, nbr_[X1], 100, comm_cart_, &send_req_[1]);
    MPI_Isend(&operator()(0, 0, 0),    1, FaceY, nbr_[Y0], 103, comm_cart_, &send_req_[2]);
    MPI_Isend(&operator()(0, Ny-1, 0), 1, FaceY, nbr_[Y1], 102, comm_cart_, &send_req_[3]);
    MPI_Isend(&operator()(0, 0, 0),    1, FaceZ, nbr_[Z0], 105, comm_cart_, &send_req_[4]);
    MPI_Isend(&operator()(0, 0, Nz-1), 1, FaceZ, nbr_[Z1], 104, comm_cart_, &send_req_[5]);
    // clang-format on
}

void LaplacianSmootherMPI::sync_()
{
    // wait for pending sends (because we will write to it)
    MPI_Waitall(6, send_req_.data(), MPI_STATUSES_IGNORE);

    // wait for pending recvs (because we will read from it)
    MPI_Waitall(6, recv_req_.data(), MPI_STATUSES_IGNORE);
}
