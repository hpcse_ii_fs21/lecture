// File       : LaplacianSmootherMPI_pack.cpp
// Created    : Wed Apr 14 2021 03:01:05 PM (+0200)
// Description: Laplacian smoother implementation
//              (distributed, explicit packing)
// Copyright 2021 ETH Zurich. All Rights Reserved.

#include "LaplacianSmootherMPI_pack.h"
#include <cassert>
#include <chrono>
#include <cstring>
#include <fstream>
#include <string>

using namespace std::chrono;

#define MEASURE(func, timer)                                                   \
    do {                                                                       \
        const auto t_start = steady_clock::now();                              \
        func;                                                                  \
        const auto t_end = steady_clock::now();                                \
        timer += duration_cast<microseconds>(t_end - t_start).count();         \
    } while (0)

LaplacianSmootherMPI::LaplacianSmootherMPI(const int Nx,
                                           const int Ny,
                                           const int Nz,
                                           const int Px,
                                           const int Py,
                                           const int Pz,
                                           const MPI_Comm comm_root)
    : LaplacianSmoother(Nx, Ny, Nz), comm_world_(comm_root),
      comm_cart_(MPI_COMM_NULL), procs_{Px, Py, Pz}, t_pack_(0), t_unpack_(0),
      face_X0_(Ny * Nz * 1), face_Y0_(Nx * Nz * 1), face_Z0_(Nx * Ny * 1),
      face_X1_(Ny * Nz * 1), face_Y1_(Nx * Nz * 1), face_Z1_(Nx * Ny * 1),
      recv_req_(6, MPI_REQUEST_NULL), send_req_(6, MPI_REQUEST_NULL)
{
    int size;
    MPI_Comm_size(comm_world_, &size);
    if (size != Px * Py * Pz) {
        MPI_Abort(comm_world_, 1);
    }
    const int periodic[3] = {true, true, true};
    MPI_Cart_create(comm_world_, 3, procs_, periodic, true, &comm_cart_);
    MPI_Comm_rank(comm_cart_, &rank_cart_);
    MPI_Cart_shift(comm_cart_, 0, 1, &nbr_[X0], &nbr_[X1]);
    MPI_Cart_shift(comm_cart_, 1, 1, &nbr_[Y0], &nbr_[Y1]);
    MPI_Cart_shift(comm_cart_, 2, 1, &nbr_[Z0], &nbr_[Z1]);
}

LaplacianSmootherMPI::~LaplacianSmootherMPI()
{
    MPI_Comm_free(&comm_cart_);
}

void LaplacianSmootherMPI::report() const
{
    int size;
    MPI_Comm_size(comm_cart_, &size);
    std::vector<double> t_sweep(size, 0.0);
    std::vector<double> t_async(size, 0.0);
    std::vector<double> t_inner(size, 0.0);
    std::vector<double> t_ghosts(size, 0.0);
    std::vector<double> t_boundary(size, 0.0);
    std::vector<double> t_pack(size, 0.0);
    std::vector<double> t_unpack(size, 0.0);
    // clang-format off
    MPI_Gather(&t_sweep_, 1, MPI_DOUBLE, t_sweep.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    MPI_Gather(&t_async_, 1, MPI_DOUBLE, t_async.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    MPI_Gather(&t_inner_, 1, MPI_DOUBLE, t_inner.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    MPI_Gather(&t_ghosts_, 1, MPI_DOUBLE, t_ghosts.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    MPI_Gather(&t_boundary_, 1, MPI_DOUBLE, t_boundary.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    MPI_Gather(&t_pack_, 1, MPI_DOUBLE, t_pack.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    MPI_Gather(&t_unpack_, 1, MPI_DOUBLE, t_unpack.data(), 1, MPI_DOUBLE, 0, comm_cart_);
    // clang-format on

    if (0 == rank_cart_) {
        // min-max-avg
        // sweep, async, inner, ghosts, boundary
        const std::string header = "sweep\tasync\tinner\tghosts\tboundary\n";
        constexpr size_t ntimer = 7;
        double mma[ntimer][3] = {{1.0e12, 0, 0},
                                 {1.0e12, 0, 0},
                                 {1.0e12, 0, 0},
                                 {1.0e12, 0, 0},
                                 {1.0e12, 0, 0},
                                 {1.0e12, 0, 0},
                                 {1.0e12, 0, 0}};
        double *measure[ntimer] = {t_sweep.data(),
                                   t_async.data(),
                                   t_inner.data(),
                                   t_ghosts.data(),
                                   t_boundary.data(),
                                   t_pack.data(),
                                   t_unpack.data()};

        std::ofstream per_rank("rank_stats_" + std::to_string(procs_[0]) +
                               std::to_string(procs_[1]) +
                               std::to_string(procs_[2]) + ".dat");
        per_rank << "# rank\t" << header;
        for (int i = 0; i < size; ++i) {
            per_rank << i;
            for (int j = 0; j < ntimer; ++j) {
                measure[j][i] /= sweep_count_;
                per_rank << '\t' << measure[j][i];
                // clang-format off
                mma[j][0] = (measure[j][i] < mma[j][0]) ? measure[j][i] : mma[j][0];
                mma[j][1] = (measure[j][i] > mma[j][1]) ? measure[j][i] : mma[j][1];
                mma[j][2] += measure[j][i];
                // clang-format on
            }
            per_rank << '\n';
        }
        for (int j = 0; j < ntimer; ++j) {
            mma[j][2] /= size;
        }

        const int Nx = N_[0] - 2;
        const int Ny = N_[1] - 2;
        const int Nz = N_[2] - 2;
        // clang-format off
        std::printf("Measurement report for dimension %d x %d x %d per rank. Topology %d x %d x %d:\n", Nx, Ny, Nz, procs_[0], procs_[1], procs_[2] );
        std::printf("\tSweep count:           %zu\n", sweep_count_);
        std::printf("\tAvg. time sweep:       min:%.3e, max:%.3e, avg:%.3e microseconds\n",                        mma[0][0], mma[0][1], mma[0][2]);
        std::printf("\tAvg. time async comm:  min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%% (memcpy %.2f%%)]\n", mma[1][0], mma[1][1], mma[1][2], 100*mma[1][2]/mma[0][2], 100*mma[5][2]/mma[0][2]);
        std::printf("\tAvg. time inner:       min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%%]\n",                mma[2][0], mma[2][1], mma[2][2], 100*mma[2][2]/mma[0][2]);
        std::printf("\tAvg. time sync ghosts: min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%% (memcpy %.2f%%)]\n", mma[3][0], mma[3][1], mma[3][2], 100*mma[3][2]/mma[0][2], 100*mma[6][2]/mma[0][2]);
        std::printf("\tAvg. time boundary:    min:%.3e, max:%.3e, avg:%.3e microseconds[%.2f%%]\n",                mma[4][0], mma[4][1], mma[4][2], 100*mma[4][2]/mma[0][2]);
        // clang-format on
    }
}

void LaplacianSmootherMPI::progress_()
{
    int flag[6];
    // You can never assume automatic progress of asynchronous MPI
    // communication!  The reasons are complex (e.g. is the MPI implementation
    // tuned for special hardware? NICs that support CPU offloading; are there
    // CPU threads available to be used for communication progress instead of
    // computation, etc.).  In many cases, MPI will only make progress of
    // pending messages if a corresponding call to MPI_Wait or MPI_Test is made.
    // One software based strategy to ensure message progression is called
    // `polling`.  In polling we periodically call MPI_Test to wake up threads
    // (which involves the OS) and enforce progress of pending messages.  This
    // is what we do here.
    MPI_Testall(6, recv_req_.data(), flag, MPI_STATUSES_IGNORE);
}

void LaplacianSmootherMPI::comm_()
{
    // prepare send buffers
    MEASURE(pack_faces_(), t_pack_);

    // clang-format off
    // receive of 6 faces into coalesced memory
    MPI_Irecv(face_X0_.recv.data(), face_X0_.recv.size(), MPIDataType, nbr_[X0], 100, comm_cart_, &recv_req_[0]);
    MPI_Irecv(face_X1_.recv.data(), face_X1_.recv.size(), MPIDataType, nbr_[X1], 101, comm_cart_, &recv_req_[1]);
    MPI_Irecv(face_Y0_.recv.data(), face_Y0_.recv.size(), MPIDataType, nbr_[Y0], 102, comm_cart_, &recv_req_[2]);
    MPI_Irecv(face_Y1_.recv.data(), face_Y1_.recv.size(), MPIDataType, nbr_[Y1], 103, comm_cart_, &recv_req_[3]);
    MPI_Irecv(face_Z0_.recv.data(), face_Z0_.recv.size(), MPIDataType, nbr_[Z0], 104, comm_cart_, &recv_req_[4]);
    MPI_Irecv(face_Z1_.recv.data(), face_Z1_.recv.size(), MPIDataType, nbr_[Z1], 105, comm_cart_, &recv_req_[5]);

    // send of 6 faces from coalesced memory
    MPI_Isend(face_X0_.send.data(), face_X0_.send.size(), MPIDataType, nbr_[X0], 101, comm_cart_, &send_req_[0]);
    MPI_Isend(face_X1_.send.data(), face_X1_.send.size(), MPIDataType, nbr_[X1], 100, comm_cart_, &send_req_[1]);
    MPI_Isend(face_Y0_.send.data(), face_Y0_.send.size(), MPIDataType, nbr_[Y0], 103, comm_cart_, &send_req_[2]);
    MPI_Isend(face_Y1_.send.data(), face_Y1_.send.size(), MPIDataType, nbr_[Y1], 102, comm_cart_, &send_req_[3]);
    MPI_Isend(face_Z0_.send.data(), face_Z0_.send.size(), MPIDataType, nbr_[Z0], 105, comm_cart_, &send_req_[4]);
    MPI_Isend(face_Z1_.send.data(), face_Z1_.send.size(), MPIDataType, nbr_[Z1], 104, comm_cart_, &send_req_[5]);
    // clang-format on
}

void LaplacianSmootherMPI::sync_()
{
    // wait for pending sends (because we will write to it)
    MPI_Waitall(6, send_req_.data(), MPI_STATUSES_IGNORE);

    // wait for pending recvs (because we will read from it)
    MPI_Waitall(6, recv_req_.data(), MPI_STATUSES_IGNORE);

    // unpack receive buffers
    MEASURE(unpack_faces_(), t_unpack_);
}

void LaplacianSmootherMPI::pack_faces_()
{
    // explicitly pack send buffers (from inner domain)
    const int Nx = N_[0] - 2;
    const int Ny = N_[1] - 2;
    const int Nz = N_[2] - 2;

    // j-k faces
    const int i0 = 0;
    const int iN = Nx - 1;
    size_t idx_flat = 0;
    for (int k = 0; k < Nz; ++k) {
        for (int j = 0; j < Ny; ++j) {
            face_X0_.send[idx_flat] = operator()(i0, j, k);
            face_X1_.send[idx_flat] = operator()(iN, j, k);
            ++idx_flat;
        }
    }

#ifdef _UNROLL_
    // implicitly assumed that Ny and Nz are an integer multiple of 4!

    // i-k faces
    const int j0 = 0;
    const int jN = Ny - 1;
    idx_flat = 0;
    for (int k = 0; k < Nz; k += 4) {
        // clang-format off
        std::memcpy(&face_Y0_.send[idx_flat + 0 * Nx], &operator()(0, j0, k + 0), Nx * sizeof(DataType));
        std::memcpy(&face_Y1_.send[idx_flat + 0 * Nx], &operator()(0, jN, k + 0), Nx * sizeof(DataType));
        std::memcpy(&face_Y0_.send[idx_flat + 1 * Nx], &operator()(0, j0, k + 1), Nx * sizeof(DataType));
        std::memcpy(&face_Y1_.send[idx_flat + 1 * Nx], &operator()(0, jN, k + 1), Nx * sizeof(DataType));
        std::memcpy(&face_Y0_.send[idx_flat + 2 * Nx], &operator()(0, j0, k + 2), Nx * sizeof(DataType));
        std::memcpy(&face_Y1_.send[idx_flat + 2 * Nx], &operator()(0, jN, k + 2), Nx * sizeof(DataType));
        std::memcpy(&face_Y0_.send[idx_flat + 3 * Nx], &operator()(0, j0, k + 3), Nx * sizeof(DataType));
        std::memcpy(&face_Y1_.send[idx_flat + 3 * Nx], &operator()(0, jN, k + 3), Nx * sizeof(DataType));
        // clang-format on
        idx_flat += 4 * Nx;
    }

    // i-j faces
    const int k0 = 0;
    const int kN = Nz - 1;
    idx_flat = 0;
    for (int j = 0; j < Ny; j += 4) {
        // clang-format off
        std::memcpy(&face_Z0_.send[idx_flat + 0 * Nx], &operator()(0, j + 0, k0), Nx * sizeof(DataType));
        std::memcpy(&face_Z1_.send[idx_flat + 0 * Nx], &operator()(0, j + 0, kN), Nx * sizeof(DataType));
        std::memcpy(&face_Z0_.send[idx_flat + 1 * Nx], &operator()(0, j + 1, k0), Nx * sizeof(DataType));
        std::memcpy(&face_Z1_.send[idx_flat + 1 * Nx], &operator()(0, j + 1, kN), Nx * sizeof(DataType));
        std::memcpy(&face_Z0_.send[idx_flat + 2 * Nx], &operator()(0, j + 2, k0), Nx * sizeof(DataType));
        std::memcpy(&face_Z1_.send[idx_flat + 2 * Nx], &operator()(0, j + 2, kN), Nx * sizeof(DataType));
        std::memcpy(&face_Z0_.send[idx_flat + 3 * Nx], &operator()(0, j + 3, k0), Nx * sizeof(DataType));
        std::memcpy(&face_Z1_.send[idx_flat + 3 * Nx], &operator()(0, j + 3, kN), Nx * sizeof(DataType));
        // clang-format on
        idx_flat += 4 * Nx;
    }
#else
    // i-k faces
    const int j0 = 0;
    const int jN = Ny - 1;
    idx_flat = 0;
    for (int k = 0; k < Nz; ++k) {
        // clang-format off
        std::memcpy(&face_Y0_.send[idx_flat], &operator()(0, j0, k), Nx * sizeof(DataType));
        std::memcpy(&face_Y1_.send[idx_flat], &operator()(0, jN, k), Nx * sizeof(DataType));
        // clang-format on
        idx_flat += Nx;
    }

    // i-j faces
    const int k0 = 0;
    const int kN = Nz - 1;
    idx_flat = 0;
    for (int j = 0; j < Ny; ++j) {
        // clang-format off
        std::memcpy(&face_Z0_.send[idx_flat], &operator()(0, j, k0), Nx * sizeof(DataType));
        std::memcpy(&face_Z1_.send[idx_flat], &operator()(0, j, kN), Nx * sizeof(DataType));
        // clang-format on
        idx_flat += Nx;
    }
#endif /* _UNROLL_ */
}

void LaplacianSmootherMPI::unpack_faces_()
{
    // explicitly unpack receive buffers (into ghost buffers)
    const int Nx = N_[0] - 2;
    const int Ny = N_[1] - 2;
    const int Nz = N_[2] - 2;

    // j-k faces
    const int i0 = -1;
    const int iN = Nx;
    size_t idx_flat = 0;
    for (int k = 0; k < Nz; ++k) {
        for (int j = 0; j < Ny; ++j) {
            operator()(i0, j, k) = face_X0_.recv[idx_flat];
            operator()(iN, j, k) = face_X1_.recv[idx_flat];
            ++idx_flat;
        }
    }

#ifdef _UNROLL_
    // implicitly assumed that Ny and Nz are an integer multiple of 4!

    // i-k faces
    const int j0 = -1;
    const int jN = Ny;
    idx_flat = 0;
    for (int k = 0; k < Nz; k += 4) {
        // clang-format off
        std::memcpy(&operator()(0, j0, k + 0), &face_Y0_.recv[idx_flat + 0 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, jN, k + 0), &face_Y1_.recv[idx_flat + 0 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, j0, k + 1), &face_Y0_.recv[idx_flat + 1 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, jN, k + 1), &face_Y1_.recv[idx_flat + 1 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, j0, k + 2), &face_Y0_.recv[idx_flat + 2 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, jN, k + 2), &face_Y1_.recv[idx_flat + 2 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, j0, k + 3), &face_Y0_.recv[idx_flat + 3 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, jN, k + 3), &face_Y1_.recv[idx_flat + 3 * Nx], Nx * sizeof(DataType));
        // clang-format on
        idx_flat += 4 * Nx;
    }

    // i-j faces
    const int k0 = -1;
    const int kN = Nz;
    idx_flat = 0;
    for (int j = 0; j < Ny; j += 4) {
        // clang-format off
        std::memcpy(&operator()(0, j + 0, k0), &face_Z0_.recv[idx_flat + 0 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, j + 0, kN), &face_Z1_.recv[idx_flat + 0 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, j + 1, k0), &face_Z0_.recv[idx_flat + 1 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, j + 1, kN), &face_Z1_.recv[idx_flat + 1 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, j + 2, k0), &face_Z0_.recv[idx_flat + 2 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, j + 2, kN), &face_Z1_.recv[idx_flat + 2 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, j + 3, k0), &face_Z0_.recv[idx_flat + 3 * Nx], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, j + 3, kN), &face_Z1_.recv[idx_flat + 3 * Nx], Nx * sizeof(DataType));
        // clang-format on
        idx_flat += 4 * Nx;
    }
#else
    // i-k faces
    const int j0 = -1;
    const int jN = Ny;
    idx_flat = 0;
    for (int k = 0; k < Nz; ++k) {
        // clang-format off
        std::memcpy(&operator()(0, j0, k), &face_Y0_.recv[idx_flat], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, jN, k), &face_Y1_.recv[idx_flat], Nx * sizeof(DataType));
        // clang-format on
        idx_flat += Nx;
    }

    // i-j faces
    const int k0 = -1;
    const int kN = Nz;
    idx_flat = 0;
    for (int j = 0; j < Ny; ++j) {
        // clang-format off
        std::memcpy(&operator()(0, j, k0), &face_Z0_.recv[idx_flat], Nx * sizeof(DataType));
        std::memcpy(&operator()(0, j, kN), &face_Z1_.recv[idx_flat], Nx * sizeof(DataType));
        // clang-format on
        idx_flat += Nx;
    }
#endif /* _UNROLL_ */
}
