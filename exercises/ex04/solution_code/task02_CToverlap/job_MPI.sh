#!/usr/bin/env bash
#BSUB -W 01:00
#BSUB -J CT_overlap
#BSUB -o CT_overlap_%J.out
#BSUB -n 48

unset LSB_AFFINITY_HOSTFILE
root=$(pwd -P)
PATH="${root}":$PATH

# vanilla MPI executable
# exe=${root}/build/mainMPI

# explicit message packaging and enforcement of progress
exe=${root}/build/mainMPIpack

run_OMP12() {
    c="${1}"; shift
    dir="c${c}"
    parent=$(pwd -P)
    mkdir -p ${dir} && cd ${dir}
    export OMP_NUM_THREADS=12
    mpirun -np 1 --map-by ppr:1:socket:PE=12 --report-bindings ${exe} ${c} ${c} ${c} 1 1 1 &>"1.dat"
    mpirun -np 2 --map-by ppr:1:socket:PE=12 --report-bindings ${exe} ${c} ${c} ${c} 1 1 2 &>"2.dat"
    mpirun -np 4 --map-by ppr:1:socket:PE=12 --report-bindings ${exe} ${c} ${c} ${c} 1 2 2 &>"4.dat"
    cd ${parent}
}

mkdir -p ${LSB_JOBID}/omp12 && cd ${LSB_JOBID}/omp12
run_OMP12 128
run_OMP12 256
run_OMP12 512
run_OMP12 1024

exit 0
