#!/usr/bin/env bash
#BSUB -W 00:15
#BSUB -J p2p_OCN_benchmark
#BSUB -o p2p_OCN_benchmark_%J.out
#BSUB -n 24

unset LSB_AFFINITY_HOSTFILE
lshosts -w "$(hostname)"
lspci | grep Mellanox

mpirun -np 2 --tune mpi_mca.conf --map-by ppr:1:core --report-bindings \
    ./bw_bench results_OCN_${LSB_JOBID}.dat
code=$?

exit $code
