#!/usr/bin/env bash

# `-R fullnode` because we want to avoid contention with other users
bsub -R fullnode <job_OCN.sh
bsub -R fullnode <job_SAN.sh
