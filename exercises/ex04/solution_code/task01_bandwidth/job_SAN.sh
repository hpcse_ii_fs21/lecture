#!/usr/bin/env bash
#BSUB -W 00:15
#BSUB -J p2p_SAN_benchmark
#BSUB -o p2p_SAN_benchmark_%J.out
#BSUB -n 48

unset LSB_AFFINITY_HOSTFILE
lshosts -w "$(hostname)"
lspci | grep Mellanox

mpirun -np 2 --tune mpi_mca.conf --map-by ppr:1:node --report-bindings \
    ./bw_bench results_SAN_${LSB_JOBID}.dat
code=$?

exit $code
