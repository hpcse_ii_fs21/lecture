// File       : device_properties.cu
// Created    : Fri Apr 30 2021 10:10:45 AM (+0200)
// Author     : Fabian Wermelinger
// Description: Query GPU device properties
// Copyright 2021 ETH Zurich. All Rights Reserved.
#include "utils.h"
#include <iostream>

int main(void)
{
    const int dev = 0; // query the first device
    cudaDeviceProp prop;
    CUDA_CHECK(cudaGetDeviceProperties(&prop, dev));

    // write properties to stdout
    // clang-format off
    std::cout << "Device:             " << prop.name << std::endl;
    std::cout << "Compute capability: " << prop.major << "." << prop.minor << std::endl;
    std::cout << "SMs:                " << prop.multiProcessorCount << std::endl;
    std::cout << "GPU clock rate:     " << prop.clockRate << std::endl;
    std::cout << "Memory clock:       " << prop.memoryClockRate << std::endl;
    std::cout << "Memory bus width:   " << prop.memoryBusWidth << std::endl;
    // clang-format on

    return 0;
}
