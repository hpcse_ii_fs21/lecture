// File       : fpeak.cu
// Created    : Sat May 01 2021 09:57:06 AM (+0200)
// Author     : Fabian Wermelinger
// Description: Floating point peak performance kernel
// Copyright 2021 ETH Zurich. All Rights Reserved.

#include "CUDATimer.cuh"
#include "utils.h"
#include <cstdlib>
#include <stdio.h>
#include <vector>

#define NLOOP (1 << 16)
#define FLOP 8
#define BLOCKS 8192
#define THREADS 1024

typedef float Real;
// Pascal
#define PMAX (56 * 64 * 1.328500 * 2) // gigaflop
#define my_fma(x, y, z) __fmaf_rd(x, y, z)

__global__ void fma_kernel(Real *const store)
{
    const int tid = blockIdx.x * blockDim.x + threadIdx.x;

    register Real x1 = 0.00006f, x2 = 0.00003f, x3 = 0.00001f, x4 = 0.00005f;
    register Real y1 = 0.01f, y2 = 0.02f, y3 = 0.03f, y4 = 0.04f;

#pragma unroll 256
    for (unsigned int i = 0; i < NLOOP; ++i) {
        y1 = my_fma(y1, x2, x1);
        y2 = my_fma(y2, x3, x2);
        y3 = my_fma(y3, x1, x3);
        y4 = my_fma(y4, x2, x4);
    }

    store[tid] = y1 + y2 + y3 + y4;
}

int main(void)
{
    Real *dA;
    CUDA_CHECK(cudaMalloc((void **)&dA, BLOCKS * THREADS * sizeof(Real)));

    GPUtimer timer;
    timer.start();
    fma_kernel<<<BLOCKS, THREADS>>>(dA);
    timer.stop();

    // check result
    CUDA_CHECK(cudaDeviceSynchronize());
    std::vector<Real> result(BLOCKS * THREADS);
    CUDA_CHECK(cudaMemcpy(result.data(),
                          dA,
                          result.size() * sizeof(Real),
                          cudaMemcpyDeviceToHost));
    Real sum = 0;
    for (size_t i = 0; i < result.size(); ++i) {
        sum += result[i];
    }
    CUDA_CHECK(cudaFree(dA));

    const double kTime = timer.elapsed() * 1.0e-3; // seconds
    const double Gflop = static_cast<double>(FLOP * NLOOP) *
                         static_cast<double>(BLOCKS * THREADS) * 1.0e-9;
    const double Perf = Gflop / kTime; // Gflop/s
    const double frac = Perf / PMAX;   // fraction of peak
    printf("Performance: %f Gflops (%4.1f%% of Peak)\n", Perf, frac * 100);
    printf("Kernel Time: %f s\n", kTime);
    printf("Result: %e\n", sum);

    return 0;
}
