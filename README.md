# Course Information

Homepage: https://www.cse-lab.ethz.ch/teaching/hpcse-ii_fs21/

Zoom Information:
> https://ethz.zoom.us/j/98605505968

# Lecture Schedule

> * Exercise Monday 10:00-12:00
> * Lecture  Monday 14:00-16:00

| When       | What                                    |
|------------|-----------------------------------------|
| 22.02.2021 | Introduction (PK)                       |
| 01.03.2021 | UQ Part I - Laplace Methods  (PK)       |
| 08.03.2021 | UQ Part II - Priors , MCMC (PK)         |
| 15.03.2021 | The Korali Framework (SM/GA)            |
| 22.03.2021 | Advanced MPI (SM)                       |
| 29.03.2021 | Communication Tolerant Programming (SM) |
| 12.04.2021 | High-Throughput Computing (SM)          |
| 26.04.2021 | GPU Programming I (SM)                  |
| 03.05.2021 | Particle Methods (PK)                   |
| 10.05.2021 | GPU Programming II (SM)                 |
| 17.05.2021 | GPU Programming III (SM)                |
| 31.05.2021 | Recap and Mock Exam                     |


# Exercises Schedule

See [`exercises`](./exercises/README.md)


# Tutorials Schedule

See [`tutorials`](./tutorials/README.md)


# Contacts

* Athena:   <eceva@ethz.ch>
* Fabian:   <fabianw@mavt.ethz.ch>
* Ivica:    <kicici@ethz.ch>
* Pantelis: <pvlachas@ethz.ch>
* Sergio:   <martiser@ethz.ch>
* Tobia:    <tobiac@student.ethz.ch>
